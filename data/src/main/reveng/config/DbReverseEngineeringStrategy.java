package com.toll.data.codegen;

import org.hibernate.cfg.reveng.DelegatingReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.ReverseEngineeringStrategy;
import org.hibernate.cfg.reveng.ReverseEngineeringStrategyUtil;
import org.hibernate.cfg.reveng.TableIdentifier;
import org.hibernate.mapping.Column;
import org.hibernate.mapping.ForeignKey;
import org.hibernate.util.StringHelper;

import java.beans.Introspector;
import java.util.List;

public class DbReverseEngineeringStrategy extends DelegatingReverseEngineeringStrategy {

    private static final String ENTITY_SUFFIX = "Base";

   public DbReverseEngineeringStrategy( ReverseEngineeringStrategy delegate) {
      super(delegate);
   }

   @Override
   public String tableToClassName(TableIdentifier tableIdentifier) {
      String defaultName = super.tableToClassName(tableIdentifier);

      return defaultName + ENTITY_SUFFIX;
   }

   @Override
   public String foreignKeyToCollectionName(String keyname, TableIdentifier fromTable, List fromColumns, TableIdentifier referencedTable, List referencedColumns, boolean uniqueReference)
   {
     String propertyName = Introspector.decapitalize(StringHelper.unqualify(super.tableToClassName(fromTable)));
     propertyName = pluralize(propertyName);
     if (!uniqueReference) {
       if ((fromColumns != null) && (fromColumns.size() == 1))
       {
         String columnName = ((Column)fromColumns.get(0)).getName();
         propertyName = propertyName + "For" + toUpperCamelCase(columnName);
       }
       else
       {
         propertyName = propertyName + "For" + toUpperCamelCase(keyname);
       }
     }
     return propertyName;
   }

   @Override
   public String foreignKeyToEntityName(String keyname, TableIdentifier fromTable, List fromColumnNames, TableIdentifier referencedTable, List referencedColumnNames, boolean uniqueReference)
   {
     String propertyName = Introspector.decapitalize(StringHelper.unqualify(super.tableToClassName(referencedTable)));
     if (!uniqueReference) {
       if ((fromColumnNames != null) && (fromColumnNames.size() == 1))
       {
         String columnName = ((Column)fromColumnNames.get(0)).getName();
         propertyName = propertyName + "By" + toUpperCamelCase(columnName);
       }
       else
       {
         propertyName = propertyName + "By" + toUpperCamelCase(keyname);
       }
     }
     return propertyName;
   }

   @Override
   public String foreignKeyToManyToManyName(ForeignKey fromKey, TableIdentifier middleTable, ForeignKey toKey, boolean uniqueReference)
   {
     String propertyName = Introspector.decapitalize(StringHelper.unqualify(super.tableToClassName(TableIdentifier.create(toKey.getReferencedTable()))));
     propertyName = pluralize(propertyName);
     if (!uniqueReference) {
       if ((toKey.getColumns() != null) && (toKey.getColumns().size() == 1))
       {
         String columnName = ((Column)toKey.getColumns().get(0)).getName();
         propertyName = propertyName + "For" + toUpperCamelCase(columnName);
       }
       else
       {
         propertyName = propertyName + "For" + toUpperCamelCase(toKey.getName());
       }
     }
     return propertyName;
   }

   protected String pluralize(String singular)
   {
     return ReverseEngineeringStrategyUtil.simplePluralize(singular);
   }

   protected String toUpperCamelCase(String s)
   {
     return ReverseEngineeringStrategyUtil.toUpperCamelCase(s);
   }
}