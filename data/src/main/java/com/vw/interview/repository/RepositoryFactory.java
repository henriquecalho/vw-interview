package com.vw.interview.repository;

/**
 * Factory for building repositories
 */
public class RepositoryFactory {
  private static final Repository repositoryInstance = new Repository();

  public enum RepositoryTypes {
        HIBERNATE_SESSION
  }

  private RepositoryFactory() {
  }

  public static IRepository get() {
    return repositoryInstance;
  }

  public static IRepository get(RepositoryTypes repositoryType) {
    IRepository repository = null;
    switch (repositoryType) {
      case HIBERNATE_SESSION:
        repository = new Repository();
        break;
    }
    return repository;
  }

}
