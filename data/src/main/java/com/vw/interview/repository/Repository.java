package com.vw.interview.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.vw.interview.exception.DataException;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.querydsl.jpa.hibernate.HibernateQueryFactory;
import com.vw.interview.util.HibernateUtil;

/**
 * Repository implementation - currently hibernate used for implementation.
 */
public class Repository implements IRepository {

    private static final Logger LOG = LoggerFactory.getLogger(Repository.class);

    /** One session per repository instance and per thread */
    private final ThreadLocal<Session> threadSession = new ThreadLocal<>();

    /** Temporary container used for external session injection */
    private final ThreadLocal<Session> stashedThreadSession = new ThreadLocal<>();

    private final IVehicleSubRepository vehicleSubRepository;
    private final ICodeSubRepository codeSubRepository;
    private final IFileSubRepository fileSubRepository;
    private final IFeatureSubRepository featureSubRepository;

    public Repository() {
        this.vehicleSubRepository = new VehicleSubRepository(this);
        this.codeSubRepository = new CodeSubRepository(this);
        this.fileSubRepository = new FileSubRepository(this);
        this.featureSubRepository = new FeatureSubRepository(this);
    }

    private static Session initSession() {
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        return session;
    }

    /**
     * Very =>DANGEROUS<= method, be sure that you know what you are doing.
     * This method allows injecting an arbitrary hibernate session to be used by this repository implementation.
     * This is STRONGLY discouraged and should only be used when it's REALLY necessary to mix hibernate raw code with
     * repository (normally only for legacy code compatibility).
     *
     * Before using this method be sure that you understand this repository implementation.
     */
    private void injectSession(Session session) {

        stashedThreadSession.set(threadSession.get());
        threadSession.set(session);
    }

    private void ejectSession() {

        threadSession.set(stashedThreadSession.get());
        stashedThreadSession.remove();
    }

    public Session getSession() {

        if ( threadSession.get() == null || !threadSession.get().isOpen() ) {
            threadSession.set(initSession());
        }

        return threadSession.get();
    }

    private void closeSession() {

        Session session = threadSession.get();

        if (session != null && session.isOpen()) {
            Transaction tx = session.getTransaction();

            if (tx != null && tx.getStatus() == TransactionStatus.ACTIVE) {
                tx.rollback();
            }

            session.clear();
            session.close();
        }

        threadSession.remove();
    }

    public HibernateQueryFactory getNewQueryFactoryInstance() {
        return new HibernateQueryFactory(this.getSession());
    }

    /**
     * Starts new unit of work, if repository has already some work associated to it, an exception is thrown.
     * The use of this method should be avoided, use at your own risk.
     */
    @Override
    public IRepository startNewWork() throws DataException {

        if (hasPendingWork()) {
            throw new DataException("Not possible to start new work unit. Repository has some pendent work which should be committed first.");
        }

        closeSession();
        threadSession.set(getSession());

        return this;
    }

    @Override
    public boolean hasPendingWork() {
        boolean result = false;

        Session session = threadSession.get();

        if (session != null && session.isOpen() && session.isDirty()) {
            result = true;
        }

        return result;
    }

    @Override
    public void finishWork() {

        if (threadSession.get() != null && threadSession.get().isDirty()) {
            LOG.warn("Dangerous session closing {}", "There is pending work");
        }

        closeSession();
    }

    /** This method triggers the commit of all work performed by this repository and starts new transaction. */
    @Override
    public void commitWork() {
        Transaction tx = this.getSession().beginTransaction();
        if (tx != null && tx.getStatus() == TransactionStatus.ACTIVE) {
            tx.commit();
        }
        closeSession();
    }

    /**
     * Completely clear the session. Evict all loaded instances and cancel all pending saves, updates and deletions.
     * Do not close open iterators or instances of ScrollableResults.
     */
    @Override
    public void clearWork() {
        Session session = this.getSession();
        if (session != null) {
            session.clear();
        }
    }

    /** This method triggers the rollback of all work performed by this repository and starts new transaction. */
    @Override
    public void rollbackWork() {
        Transaction tx = this.getSession().getTransaction();
        if (tx != null && tx.getStatus() == TransactionStatus.ACTIVE) {
            tx.rollback();
        }
        closeSession();
    }

    @Override
    public <T, K extends Serializable> T get(Class<T> entityClass, K id) {
        return this.getSession().get(entityClass, id);
    }

    @Override
    public <T> void refresh(T entity) {
        this.getSession().refresh(entity);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> List<T> getAll(Class<T> entityClass) {
        return this.getSession().createCriteria(entityClass).list();
    }

    @Override
    public <T> T save(T entity) {
        this.getSession().save(entity);
        return entity;
    }

    @Override
    public <T> void update(T entity) {
        this.getSession().update(entity);
    }

    @Override
    public <T> void saveOrUpdate(T entity) { this.getSession().saveOrUpdate(entity); }

    @Override
    public <T> void delete(T entity) {
        this.getSession().delete(entity);
    }

    @Override
    public <T> void delete(List<T> entities) {
        for (T e : entities) {
            this.delete(e);
        }
    }

    @Override
    public void flush( ) {
        this.getSession().flush();
    }

    @Override
    public <T> T loadLazy(T entity) {

        if (!Hibernate.isInitialized(entity)) {
            Hibernate.initialize(entity);
        }

        return entity;
    }

    public <T> Predicate splitInList(SimpleExpression<T> expression, Collection<T> listToSplit) {

        Predicate currentPredicate = null;

        for (List<T> partitionedList : Lists.partition(new ArrayList<>(listToSplit), 1000)) {
            if (currentPredicate != null) {
                currentPredicate = expression.in(partitionedList).or(currentPredicate);
            }
            else {
                currentPredicate = expression.in(partitionedList);
            }
        }
        return currentPredicate;
    }

    @Override
    public <S> IRepository injectExternalSession(S session) throws DataException {
        try {
            injectSession((Session) session);
            return this;
        }
        catch (Exception ex) {
            throw new DataException("Wasn't possible to inject external session!!!");
        }
    }

    @Override
    public IRepository ejectExternalSession() {
        try {
            ejectSession();
        }
        catch (Exception ex) {
            LOG.error("Wasn't possible to eject external session", ex);
        }
        return this;
    }

    @Override
    public <S> S toSession(Class<S> sessionClass) { return sessionClass.cast(getSession()); }

    @Override
    public IRepository flushWork() {
        Session session = this.getSession();
        if (session != null) {
            session.flush();
        }
        return this;
    }

    @Override
    public IVehicleSubRepository vehicle() {
        return this.vehicleSubRepository;
    }
    @Override
    public ICodeSubRepository code() {
        return this.codeSubRepository;
    }
    @Override
    public IFileSubRepository file() { return this.fileSubRepository; }
    @Override
    public IFeatureSubRepository feature() { return this.featureSubRepository; }
}