package com.vw.interview.repository;

import com.vw.interview.model.Feature;
import com.vw.interview.model.FeatureDetailExistenceEnum;
import com.vw.interview.model.Vehicle;

import java.util.ArrayList;
import java.util.List;

import static com.vw.interview.model.QCode.code;
import static com.vw.interview.model.QFeature.feature;
import static com.vw.interview.model.QFeatureDetail.featureDetail;
import static com.vw.interview.model.QVehicle.vehicle;
import static com.vw.interview.model.QVehicleCodes.vehicleCodes;

/**
 * Sub-repository for feature specific operations.
 */
public class FeatureSubRepository implements IRepository.IFeatureSubRepository {

    private final Repository repository;

    public FeatureSubRepository(Repository repository) { this.repository = repository; }

    @Override
    public List<Feature> getVehicleInstallableFeatures(String vin){
        List<Feature> installableFeatures = new ArrayList<>();

        List<Feature> allFeatures = repository.getNewQueryFactoryInstance()
                .query()
                .select(feature)
                .from(feature)
                .fetch();

        for(Feature feature : allFeatures){

            Vehicle vehicleModel = repository.getNewQueryFactoryInstance()
                    .query()
                    .select(vehicle)
                    .from(vehicle)
                    .innerJoin(vehicleCodes).on(vehicleCodes.vehicle.vehicleId.eq(vehicle.vehicleId))
                    .where(vehicle.vin.eq(vin)
                    .and(vehicleCodes.code.codeId.in(repository.getNewQueryFactoryInstance()
                            .query()
                            .select(featureDetail.code.codeId)
                            .from(featureDetail)
                            .innerJoin(vehicleCodes).on(vehicleCodes.code.codeId.eq(featureDetail.code.codeId))
                            .where(featureDetail.featureDetailExistence.featureDetailExistenceId.eq(FeatureDetailExistenceEnum.REQUIRED.id())
                            .and(featureDetail.feature.featureId.eq(feature.getFeatureId())))
                        ))
                    .and(vehicleCodes.code.codeId.notIn(repository.getNewQueryFactoryInstance()
                            .query()
                            .select(featureDetail.code.codeId)
                            .from(featureDetail)
                            .innerJoin(vehicleCodes).on(vehicleCodes.code.codeId.eq(featureDetail.code.codeId))
                            .where(featureDetail.featureDetailExistence.featureDetailExistenceId.eq(FeatureDetailExistenceEnum.ABSENT.id())
                            .and(featureDetail.feature.featureId.eq(feature.getFeatureId())))
                        ))
                    )
                    .fetchOne();

            if(vehicleModel != null)
                installableFeatures.add(feature);
        }
        return installableFeatures;
    }

    @Override
    public List<Feature> getVehicleIncompatibleFeatures(String vin){
        List<Feature> features = repository.getNewQueryFactoryInstance()
                .query()
                .select(feature)
                .from(feature)
                .fetch();

        List<Feature> installableFeatures = getVehicleInstallableFeatures(vin);
        features.removeAll(installableFeatures);
        return features;
    }
}
