package com.vw.interview.repository;

import com.vw.interview.model.FileModel;
import com.vw.interview.model.FileVehicleTemp;
import com.vw.interview.model.FileVehicleTempConflict;

import java.util.List;

import static com.vw.interview.model.QFileModel.fileModel;
import static com.vw.interview.model.QFileVehicleTemp.fileVehicleTemp;
import static com.vw.interview.model.QFileVehicleTempConflict.fileVehicleTempConflict;

/**
 * Sub-repository for file specific operations.
 */
public class FileSubRepository implements IRepository.IFileSubRepository {

    private final Repository repository;

    public FileSubRepository(Repository repository) { this.repository = repository; }

    @Override
    public boolean exists(String fileName) {
        FileModel file = repository.getNewQueryFactoryInstance()
                .query()
                .select(fileModel)
                .from(fileModel)
                .where(fileModel.name.eq(fileName))
                .fetchFirst();
        return file != null;
    }

    @Override
    public List<FileVehicleTemp> getFileVehicleTemp(Integer min, Integer max){
        return repository.getNewQueryFactoryInstance()
                .query()
                .select(fileVehicleTemp)
                .from(fileVehicleTemp)
                .where(fileVehicleTemp.fileVehicleTempId.goe(min)
                .and(fileVehicleTemp.fileVehicleTempId.loe(max)))
                .fetch();
    }

    @Override
    public List<FileVehicleTempConflict> getFileVehicleTempConflict(){
        return repository.getNewQueryFactoryInstance()
                .query()
                .select(fileVehicleTempConflict)
                .from(fileVehicleTempConflict)
                .fetch();
    }

    @Override
    public long deleteFileVehicleTempConflict(Integer min, Integer max){
        return repository.getNewQueryFactoryInstance()
                .delete(fileVehicleTempConflict)
                .where(fileVehicleTempConflict.id.goe(min)
                        .and(fileVehicleTempConflict.id.loe(max)))
                .execute();
    }

    @Override
    public List<FileVehicleTempConflict> getFileVehicleTempConflict(Integer min, Integer max){
        return repository.getNewQueryFactoryInstance()
                .query()
                .select(fileVehicleTempConflict)
                .from(fileVehicleTempConflict)
                .where(fileVehicleTempConflict.id.goe(min)
                        .and(fileVehicleTempConflict.id.loe(max)))
                .fetch();
    }

    @Override
    public long deleteFileVehicleTemp(Integer min, Integer max){
        return repository.getNewQueryFactoryInstance()
                .delete(fileVehicleTemp)
                .where(fileVehicleTemp.fileVehicleTempId.goe(min)
                        .and(fileVehicleTemp.fileVehicleTempId.loe(max)))
                .execute();
    }

    @Override
    public long deleteFileVehicleTempConflict(){
        return repository.getNewQueryFactoryInstance()
                .delete(fileVehicleTempConflict)
                .execute();
    }

    @Override
    public int minFileVehicleTemp(){
        Integer min = repository.getNewQueryFactoryInstance()
                .query()
                .select(fileVehicleTemp.fileVehicleTempId.min())
                .from(fileVehicleTemp)
                .fetchOne();
        return min == null ? 0 : min;
    }

    @Override
    public int maxFileVehicleTemp(){
        Integer max = repository.getNewQueryFactoryInstance()
                .query()
                .select(fileVehicleTemp.fileVehicleTempId.max())
                .from(fileVehicleTemp)
                .fetchOne();
        return max == null ? 0 : max;
    }

    @Override
    public int minFileVehicleTempConflict(){
        Integer min = repository.getNewQueryFactoryInstance()
                .query()
                .select(fileVehicleTempConflict.id.min())
                .from(fileVehicleTempConflict)
                .fetchOne();
        return min == null ? 0 : min;
    }

    @Override
    public int maxFileVehicleTempConflict(){
        Integer max = repository.getNewQueryFactoryInstance()
                .query()
                .select(fileVehicleTempConflict.id.max())
                .from(fileVehicleTempConflict)
                .fetchOne();
        return max == null ? 0 : max;
    }

    @Override
    public long deleteFileVehicleTempDuplicates(){
        return repository.getNewQueryFactoryInstance()
                .delete(fileVehicleTemp)
                .where(fileVehicleTemp.fileVehicleTempId.notIn(repository.getNewQueryFactoryInstance()
                        .query()
                        .select(fileVehicleTemp.fileVehicleTempId.max())
                        .from(fileVehicleTemp)
                        .groupBy(fileVehicleTemp.fileTypeId, fileVehicleTemp.vin, fileVehicleTemp.codeId)
                ))
                .execute();
    }

    @Override
    public long insertDistinctVins() {
        String sql = "Insert into file_vehicle_temp_conflict (temp) select distinct(vin) from file_vehicle_temp";
        return repository.getSession()
                .createSQLQuery(sql)
                .executeUpdate();
    }

    @Override
    public long insertDistinctCodeIdsSoftware() {
        String sql = "Insert into file_vehicle_temp_conflict (temp) " +
                "select distinct(code_id) from file_vehicle_temp where file_type_id = 'SOFTWARE'";
        return repository.getSession()
                .createSQLQuery(sql)
                .executeUpdate();
    }

    @Override
    public long insertDistinctCodeIdsHardware() {
        String sql = "Insert into file_vehicle_temp_conflict (temp) " +
                "select distinct(code_id) from file_vehicle_temp where file_type_id = 'HARDWARE'";
        return repository.getSession()
                .createSQLQuery(sql)
                .executeUpdate();
    }
}
