package com.vw.interview.repository;

import com.querydsl.jpa.hibernate.HibernateQuery;
import com.vw.interview.model.Code;
import com.vw.interview.model.VehicleCodes;
import com.querydsl.core.types.dsl.BooleanExpression;

import java.util.List;

import static com.vw.interview.model.QVehicle.vehicle;
import static com.vw.interview.model.QCode.code;
import static com.vw.interview.model.QVehicleCodes.vehicleCodes;

/**
 * Sub-repository for code specific operations.
 */
public class CodeSubRepository implements IRepository.ICodeSubRepository {

    private final Repository repository;

    public CodeSubRepository(Repository repository) { this.repository = repository; }

    @Override
    public Code get(String codeId){
        return repository.getNewQueryFactoryInstance()
                .query()
                .select(code)
                .from(code)
                .where(code.codeId.eq(codeId))
                .fetchOne();
    }

    @Override
    public List<String> get(List<String> codes){
        return repository.getNewQueryFactoryInstance()
                .query()
                .select(code.codeId)
                .from(code)
                .where(code.codeId.in(codes))
                .fetch();
    }

    /**
     * Get a List of VehicleCodes give a vehicle vin and a code code.
     */
    public List<VehicleCodes> getByVinAndCode(List<VehicleCodes> vehicleCodeList){
        HibernateQuery<VehicleCodes> query = repository
                .getNewQueryFactoryInstance()
                .query()
                .select(vehicleCodes)
                .from(vehicleCodes)
                .innerJoin(vehicle).on(vehicle.vehicleId.eq(vehicleCodes.vehicle.vehicleId))
                .innerJoin(code).on(code.codeId.eq(vehicleCodes.code.codeId));

        BooleanExpression expression = vehicle.vin.isNotNull();

        vehicleCodeList.forEach(vc -> expression.or(vehicle.vin.eq(vc.getVehicle().getVin())
                        .and(code.codeId.eq(vc.getCode().getCodeId()))));

        return query.where(expression).fetch();
    }
}
