package com.vw.interview.repository;

import com.vw.interview.model.Vehicle;

import java.util.List;

import static com.vw.interview.model.QVehicle.vehicle;

/**
 * Sub-repository for vehicle specific operations.
 */
public class VehicleSubRepository implements IRepository.IVehicleSubRepository {

    private final Repository repository;

    public VehicleSubRepository(Repository repository) { this.repository = repository; }

    @Override
    public List<String> get(List<String> vins){
        return repository.getNewQueryFactoryInstance()
                .query()
                .select(vehicle.vin)
                .from(vehicle)
                .where(vehicle.vin.in(vins))
                .fetch();
    }

    @Override
    public Vehicle get(String vin){
        return repository.getNewQueryFactoryInstance()
                .query()
                .select(vehicle)
                .from(vehicle)
                .where(vehicle.vin.eq(vin))
                .fetchOne();
    }
}
