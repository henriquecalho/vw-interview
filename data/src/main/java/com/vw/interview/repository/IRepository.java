package com.vw.interview.repository;

import com.vw.interview.exception.DataException;
import com.vw.interview.model.*;

import java.io.Serializable;
import java.util.List;

/**
 * Base interface for Data Access Object (DAO), i.e. for CRUD persistence operations and common queries.
 */
public interface IRepository {

    <S> S toSession(Class<S> sessionClass);

    <S> IRepository injectExternalSession(S session) throws DataException;

    IRepository ejectExternalSession();

    IRepository flushWork();

    IRepository startNewWork() throws DataException;

    void commitWork();

    void clearWork();

    void rollbackWork();

    boolean hasPendingWork();

    void finishWork();

    <T, K extends Serializable> T get(Class<T> entityClass, K id);

    <T> void refresh(T entity);

    <T> List<T> getAll(Class<T> entityClass);

    <T> T save(T entity);

    <T> void update(T entity);

    <T> void saveOrUpdate(T entity);
    <T> void delete(T entity);

    <T> void delete(List<T> entities);

    <T> T loadLazy(T entity);

    void flush();

    interface IVehicleSubRepository {
        List<String> get(List<String> vins);
        Vehicle get(String vin);
    }

    interface ICodeSubRepository {
        Code get(String code);
        List<String> get(List<String> codes);
        List<VehicleCodes> getByVinAndCode(List<VehicleCodes> vehicleCodeList);
    }

    interface IFileSubRepository {
        boolean exists(String fileName);
        List<FileVehicleTemp> getFileVehicleTemp(Integer min, Integer max);
        long deleteFileVehicleTemp(Integer min, Integer max);
        List<FileVehicleTempConflict> getFileVehicleTempConflict(Integer min, Integer max);
        long deleteFileVehicleTempConflict(Integer min, Integer max);
        long deleteFileVehicleTempConflict();
        int minFileVehicleTemp();
        int maxFileVehicleTemp();
        int minFileVehicleTempConflict();
        int maxFileVehicleTempConflict();
        long deleteFileVehicleTempDuplicates();
        long insertDistinctVins();
        long insertDistinctCodeIdsSoftware();
        long insertDistinctCodeIdsHardware();
        List<FileVehicleTempConflict> getFileVehicleTempConflict();
    }
    interface IFeatureSubRepository {
        List<Feature> getVehicleInstallableFeatures(String vin);
        List<Feature> getVehicleIncompatibleFeatures(String vin);
    }

    IVehicleSubRepository vehicle();
    ICodeSubRepository code();
    IFileSubRepository file();
    IFeatureSubRepository feature();
}

