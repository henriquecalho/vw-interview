package com.vw.interview.util;

import com.querydsl.core.types.dsl.NumberPath;
import com.querydsl.core.types.dsl.StringPath;
import com.querydsl.jpa.hibernate.HibernateQuery;

public class QueryDslNullCoalescing<T> {

    private final HibernateQuery<T> query;

    public QueryDslNullCoalescing(HibernateQuery<T> query){
        this.query = query;
    }

    public void whereEq(StringPath path, String property){
        if (property != null) {
            query.where(path.eq(property));
        }
    }

    public void whereEq(NumberPath<Long> path, Long property){
        if (property != null) {
            query.where(path.eq(property));
        }
    }

    public void offset(Integer value){
        if (value != null) {
            query.offset(value);
        }
    }

    public void limit(Integer value){
        if (value != null) {
            query.limit(value);
        }
    }
}