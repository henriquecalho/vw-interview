package com.vw.interview.model;

/**
 * Possible values for FEATURE_DETAIL_EXISTENCE.
 */
public enum FeatureDetailExistenceEnum {

    REQUIRED("REQUIRED"),
    ABSENT("ABSENT");

    private String value;

    private FeatureDetailExistenceEnum(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public String id() {
        return this.value;
    }
}
