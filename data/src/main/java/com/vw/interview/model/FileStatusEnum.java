package com.vw.interview.model;

/**
 * Possible values for FILE_STATUS.
 */
public enum FileStatusEnum {

    CREATED("CREATED"),
    ARCHIVED("ARCHIVED"),
    ERROR("ERROR");

    private String value;

    private FileStatusEnum(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public String id() {
        return this.value;
    }
}
