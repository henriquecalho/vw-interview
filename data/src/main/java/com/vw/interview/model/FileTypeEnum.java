package com.vw.interview.model;

/**
 * Possible values for FILE_STATUS.
 */
public enum FileTypeEnum {

    HARDWARE("HARDWARE"),
    SOFTWARE("SOFTWARE");

    private String value;

    private FileTypeEnum(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public String id() {
        return this.value;
    }
}
