package com.vw.interview.model;

/**
 * Possible values for CODETYPE.
 */
public enum CodeTypeEnum {

    SOFTWARE("SOFTWARE"),
    HARDWARE("HARDWARE");

    private String value;

    private CodeTypeEnum(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public String id() {
        return this.value;
    }
}
