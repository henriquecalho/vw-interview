

-- REFDATA

insert into code_type (code_type_id, description) values ('SOFTWARE', 'Software');
insert into code_type (code_type_id, description) values ('HARDWARE', 'Hardware');

insert into feature_detail_existence (feature_detail_existence_id, description) values ('REQUIRED', 'Required');
insert into feature_detail_existence (feature_detail_existence_id, description) values ('ABSENT', 'Absent');

insert into file_status (file_status_id, description) values ('CREATED', 'Created');
insert into file_status (file_status_id, description) values ('ARCHIVED', 'Archived');
insert into file_status (file_status_id, description) values ('ERROR', 'Error');

insert into file_type (file_type_id, description) values ('HARDWARE', 'Hardware');
insert into file_type (file_type_id, description) values ('SOFTWARE', 'Software');

-- CONFIGURATION

insert into code (code_id, code_type_id) values
-- FEATURE_A
('GdS6TI', 'SOFTWARE'), ('93ZSw9', 'SOFTWARE'), ('btZUSp', 'SOFTWARE'), ('MZgsou', 'SOFTWARE'), ('Di75Ry', 'SOFTWARE'), ('0vhcNa', 'SOFTWARE'), ('33MHDf', 'SOFTWARE'), ('L34Pur', 'SOFTWARE'),
('ykzkfK', 'SOFTWARE'), ('87Zhwo', 'SOFTWARE'), ('y4XKWo', 'SOFTWARE'), ('ay0pW2', 'SOFTWARE'), ('44OmDi', 'SOFTWARE'), ('aJsd3i', 'SOFTWARE'), ('Qoflqf', 'SOFTWARE'), ('2EzZXE', 'SOFTWARE'), ('j3mmf8', 'SOFTWARE'), ('MUR8Lx', 'SOFTWARE'), ('E6GYk7', 'SOFTWARE'), ('rDJyQX', 'SOFTWARE'),
('rlTcbX', 'HARDWARE'), ('wEEA00', 'HARDWARE'), ('SoF5uL', 'HARDWARE'), ('VhB9VY', 'HARDWARE'), ('NWytcy', 'HARDWARE'),
('yZDXJJ', 'HARDWARE'), ('tMI8bI', 'HARDWARE'), ('DS8tZU', 'HARDWARE'), ('PgOtkv', 'HARDWARE'), ('PuyTwj', 'HARDWARE'), ('ObZw28', 'HARDWARE'), ('ZCLFOe', 'HARDWARE'), ('jyP5PK', 'HARDWARE'), ('pS5ZQs', 'HARDWARE'), ('rcjjPX', 'HARDWARE'), ('6VO6Uq', 'HARDWARE'), ('DAlCk4', 'HARDWARE'), ('YxKjcX', 'HARDWARE'),
-- FEATURE_B
('FhFXVE', 'SOFTWARE'), ('FVlp0N', 'SOFTWARE'), ('I25pUg', 'SOFTWARE'), ('PeQWGL', 'SOFTWARE'), ('LYZzKL', 'SOFTWARE'), ('Cd9t6T', 'SOFTWARE'), ('pYgxjp', 'SOFTWARE'), ('T55Adn', 'SOFTWARE'), ('cjKv9N', 'SOFTWARE'),
('yfepdF', 'SOFTWARE'), ('Q54BVi', 'SOFTWARE'), ('1QNx4P', 'SOFTWARE'), ('u9XCsm', 'SOFTWARE'), ('RgrAEU', 'SOFTWARE'), ('0M97HZ', 'SOFTWARE'), ('BD80qR', 'SOFTWARE'), ('RGOkrt', 'SOFTWARE'), ('LV7Msr', 'SOFTWARE'), ('0OEvxe', 'SOFTWARE'), ('VLyf6R', 'SOFTWARE'), ('s1I5dm', 'SOFTWARE'), ('I4wRf9', 'SOFTWARE'),
('fMm4Hl', 'HARDWARE'), ('PWO7oa', 'HARDWARE'), ('F73iHn', 'HARDWARE'),
('mrGqkV', 'HARDWARE'), ('Ps19N7', 'HARDWARE'),
-- FEATURE_C
('CtbvOZ', 'SOFTWARE'), ('bpdM7a', 'SOFTWARE'), ('rTM6gD', 'SOFTWARE'), ('LvXPPT', 'SOFTWARE'), ('Gwz57A', 'SOFTWARE'), ('NNTgVk', 'SOFTWARE'), ('dt5WJj', 'SOFTWARE'), ('zTUQwE', 'SOFTWARE'), ('ufX8mD', 'SOFTWARE'), ('UPCZFv', 'SOFTWARE'), ('uUZjNJ', 'SOFTWARE'), ('ljnm95', 'SOFTWARE'), ('QP8Bls', 'SOFTWARE'), ('egNmFq', 'SOFTWARE'), ('PJyE8c', 'SOFTWARE'),
('T2WuvF', 'SOFTWARE'),
('wv2CZs', 'HARDWARE'), ('CEuBzO', 'HARDWARE'), ('Cu5fGc', 'HARDWARE'), ('jvqI5i', 'HARDWARE'), ('pZLSFn', 'HARDWARE'), ('eUMxfa', 'HARDWARE'),
('JY3Vcn', 'HARDWARE'), ('8PielJ', 'HARDWARE'), ('NcOLyY', 'HARDWARE'), ('Zfahrb', 'HARDWARE'), ('iKALCh', 'HARDWARE'), ('6IHTbr', 'HARDWARE');

-- FEATURE

insert into feature (feature_id, description) values
('FEATURE_A', 'Feature A description'),
('FEATURE_B', 'Feature B description'),
('FEATURE_C', 'Feature C description');

-- FEATURE_DETAIL

insert into feature_detail (feature_id, code_id, feature_detail_existence_id) values
-- FEATURE_A
('FEATURE_A', 'GdS6TI', 'REQUIRED'), ('FEATURE_A', '93ZSw9', 'REQUIRED'), ('FEATURE_A', 'btZUSp', 'REQUIRED'), ('FEATURE_A', 'MZgsou', 'REQUIRED'), ('FEATURE_A', 'Di75Ry', 'REQUIRED'), ('FEATURE_A', '0vhcNa', 'REQUIRED'), ('FEATURE_A', '33MHDf', 'REQUIRED'), ('FEATURE_A', 'Di75Ry', 'REQUIRED'), ('FEATURE_A', 'L34Pur', 'REQUIRED'),
('FEATURE_A', 'ykzkfK', 'ABSENT'), ('FEATURE_A', '87Zhwo', 'ABSENT'), ('FEATURE_A', 'y4XKWo', 'ABSENT'), ('FEATURE_A', 'ay0pW2', 'ABSENT'), ('FEATURE_A', '44OmDi', 'ABSENT'), ('FEATURE_A', 'aJsd3i', 'ABSENT'), ('FEATURE_A', 'Qoflqf', 'ABSENT'), ('FEATURE_A', '2EzZXE', 'ABSENT'), ('FEATURE_A', 'j3mmf8', 'ABSENT'), ('FEATURE_A', 'MUR8Lx', 'ABSENT'), ('FEATURE_A', 'E6GYk7', 'ABSENT'), ('FEATURE_A', 'rDJyQX', 'ABSENT'),
('FEATURE_A', 'rlTcbX', 'REQUIRED'), ('FEATURE_A', 'wEEA00', 'REQUIRED'), ('FEATURE_A', 'SoF5uL', 'REQUIRED'), ('FEATURE_A', 'VhB9VY', 'REQUIRED'), ('FEATURE_A', 'NWytcy', 'REQUIRED'),
('FEATURE_A', 'yZDXJJ', 'REQUIRED'), ('FEATURE_A', 'tMI8bI', 'REQUIRED'), ('FEATURE_A', 'DS8tZU', 'REQUIRED'), ('FEATURE_A', 'PgOtkv', 'REQUIRED'), ('FEATURE_A', 'PuyTwj', 'REQUIRED'), ('FEATURE_A', 'ObZw28', 'REQUIRED'), ('FEATURE_A', 'ZCLFOe', 'REQUIRED'), ('FEATURE_A', 'jyP5PK', 'REQUIRED'), ('FEATURE_A', 'pS5ZQs', 'REQUIRED'), ('FEATURE_A', 'rcjjPX', 'REQUIRED'), ('FEATURE_A', '6VO6Uq', 'REQUIRED'), ('FEATURE_A', 'DAlCk4', 'REQUIRED'), ('FEATURE_A', 'YxKjcX', 'REQUIRED'),
-- FEATURE_B
('FEATURE_B', 'FhFXVE', 'REQUIRED'), ('FEATURE_B', 'FVlp0N', 'REQUIRED'), ('FEATURE_B', 'I25pUg', 'REQUIRED'), ('FEATURE_B', 'PeQWGL', 'REQUIRED'), ('FEATURE_B', 'LYZzKL', 'REQUIRED'), ('FEATURE_B', 'Cd9t6T', 'REQUIRED'), ('FEATURE_B', 'pYgxjp', 'REQUIRED'), ('FEATURE_B', 'T55Adn', 'REQUIRED'), ('FEATURE_B', 'cjKv9N', 'REQUIRED'),
('FEATURE_B', 'yfepdF', 'ABSENT'), ('FEATURE_B', 'Q54BVi', 'ABSENT'), ('FEATURE_B', '1QNx4P', 'ABSENT'), ('FEATURE_B', 'u9XCsm', 'ABSENT'), ('FEATURE_B', 'RgrAEU', 'ABSENT'), ('FEATURE_B', '0M97HZ', 'ABSENT'), ('FEATURE_B', 'BD80qR', 'ABSENT'), ('FEATURE_B', 'RGOkrt', 'ABSENT'), ('FEATURE_B', 'y4XKWo', 'ABSENT'), ('FEATURE_B', 'LV7Msr', 'ABSENT'), ('FEATURE_B', '0OEvxe', 'ABSENT'), ('FEATURE_B', 'VLyf6R', 'ABSENT'), ('FEATURE_B', 's1I5dm', 'ABSENT'), ('FEATURE_B', 'I4wRf9', 'ABSENT'),
('FEATURE_B', 'fMm4Hl', 'REQUIRED'), ('FEATURE_B', 'PWO7oa', 'REQUIRED'), ('FEATURE_B', 'F73iHn', 'REQUIRED'),
('FEATURE_B', 'mrGqkV', 'ABSENT'), ('FEATURE_B', 'Ps19N7', 'ABSENT'),
-- FEATURE_C
('FEATURE_C', 'CtbvOZ', 'REQUIRED'), ('FEATURE_C', 'bpdM7a', 'REQUIRED'), ('FEATURE_C', 'rTM6gD', 'REQUIRED'), ('FEATURE_C', 'LvXPPT', 'REQUIRED'), ('FEATURE_C', 'Gwz57A', 'REQUIRED'), ('FEATURE_C', 'NNTgVk', 'REQUIRED'), ('FEATURE_C', 'dt5WJj', 'REQUIRED'), ('FEATURE_C', 'zTUQwE', 'REQUIRED'), ('FEATURE_C', 'ufX8mD', 'REQUIRED'), ('FEATURE_C', '0vhcNa', 'REQUIRED'), ('FEATURE_C', 'UPCZFv', 'REQUIRED'), ('FEATURE_C', 'uUZjNJ', 'REQUIRED'), ('FEATURE_C', 'ljnm95', 'REQUIRED'), ('FEATURE_C', 'QP8Bls', 'REQUIRED'), ('FEATURE_C', 'egNmFq', 'REQUIRED'), ('FEATURE_C', 'Gwz57A', 'REQUIRED'), ('FEATURE_C', 'PJyE8c', 'REQUIRED'), ('FEATURE_C', 'pYgxjp', 'REQUIRED'),
('FEATURE_C', 'T2WuvF', 'ABSENT'),
('FEATURE_C', 'wv2CZs', 'REQUIRED'), ('FEATURE_C', 'CEuBzO', 'REQUIRED'), ('FEATURE_C', 'Cu5fGc', 'REQUIRED'), ('FEATURE_C', 'jvqI5i', 'REQUIRED'), ('FEATURE_C', 'pZLSFn', 'REQUIRED'), ('FEATURE_C', 'eUMxfa', 'REQUIRED'),
('FEATURE_C', 'JY3Vcn', 'ABSENT'), ('FEATURE_C', '8PielJ', 'ABSENT'), ('FEATURE_C', 'NcOLyY', 'ABSENT'), ('FEATURE_C', 'Zfahrb', 'ABSENT'), ('FEATURE_C', 'iKALCh', 'ABSENT'), ('FEATURE_C', '6IHTbr', 'ABSENT');


