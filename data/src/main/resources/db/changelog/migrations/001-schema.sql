

-- REFDATA

create table code_type (
	code_type_id   varchar(20) primary key,
	description    varchar(100)
);

create table feature_detail_existence (
	feature_detail_existence_id varchar(20) primary key,
	description                 varchar(100)
);

create table file_status (
	file_status_id  varchar(20) primary key,
	description     varchar(100)
);

create table file_type (
	file_type_id    varchar(20) primary key,
	description     varchar(100)
);

------------------------------------------------------------------------------------------------------------------------

create table vehicle (
    vehicle_id      serial      primary key,
    vin             varchar(17) unique not null,
    creation_date   timestamp   default current_timestamp not null
);

create table code (
	code_id                 varchar(6)  primary key,
	code_type_id            varchar(20) not null,
    creation_date           timestamp   default current_timestamp not null,
    foreign key (code_type_id) references code_type (code_type_id)
);

create table vehicle_codes (
    vehicle_codes_id            serial primary key,
    vehicle_id                  int         not null,
    code_id                     varchar(20) not null,
    creation_date               timestamp   default current_timestamp not null,
    unique(vehicle_id, code_id),
    foreign key (vehicle_id)    references vehicle  (vehicle_id),
    foreign key (code_id)       references code     (code_id)
);

create table feature (
	feature_id      varchar(20) primary key,
	description     varchar(100),
    creation_date   timestamp   default current_timestamp not null
);

create table feature_detail (
	feature_detail_id           serial      primary key,
	feature_id                  varchar(20) not null,
	code_id                     varchar(20) not null,
	feature_detail_existence_id varchar(20) not null,
    foreign key (feature_id)                    references feature                  (feature_id),
    foreign key (code_id)                       references code                     (code_id),
    foreign key (feature_detail_existence_id)   references feature_detail_existence (feature_detail_existence_id)
);

create table file (
    file_id         serial          primary key,
    file_type_id    varchar(20)     not null,
    file_status_id  varchar(20)     not null,
    name            varchar(20)     unique not null,
    path            varchar(100)    not null,
    creation_date   timestamp       default current_timestamp not null,
    foreign key (file_type_id)      references file_type    (file_type_id),
    foreign key (file_status_id)    references file_status  (file_status_id)
);

create table file_vehicle_temp (
    file_vehicle_temp_id   serial      primary key,
    file_type_id            varchar(20),
    vin                     varchar(17),
	code_id                 varchar(6)
);

create table file_vehicle_temp_conflict (
    id  serial primary key,
    temp varchar(20)
);
