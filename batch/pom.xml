<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>vw.interview</groupId>
    <artifactId>batch</artifactId>
    <version>1.0-SNAPSHOT</version>

    <name>Spring Batch Admin</name>

    <properties>
        <java.version>1.8</java.version>
        <maven.compiler.source>${java.version}</maven.compiler.source>
        <maven.compiler.target>${java.version}</maven.compiler.target>
        <spring.boot.starter.version>1.5.18.RELEASE</spring.boot.starter.version>
        <spring-batch-admin-manager.version>1.3.1.RELEASE</spring-batch-admin-manager.version>
        <org.testcontainers.version>1.12.5</org.testcontainers.version>
    </properties>

    <!-- Add spring-boot-dependencies instead of adding spring-boot-starter-parent -->
<!--
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-dependencies</artifactId>
                <version>${spring.boot.starter.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
-->

    <dependencies>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>4.3.21.RELEASE</version>
        </dependency>

        <!-- Spring -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-batch</artifactId>
            <version>${spring.boot.starter.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>${spring.boot.starter.version}</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-devtools</artifactId>
            <version>${spring.boot.starter.version}</version>
            <scope>runtime</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-oxm</artifactId>
            <version>4.3.21.RELEASE</version>
        </dependency>
        <dependency>
            <groupId>org.springframework.batch</groupId>
            <artifactId>spring-batch-admin-manager</artifactId>
            <version>${spring-batch-admin-manager.version}</version>
            <exclusions>
                <exclusion>
                    <artifactId>slf4j-log4j12</artifactId>
                    <groupId>org.slf4j</groupId>
                </exclusion>
            </exclusions>
        </dependency>

        <!-- My Modules -->
        <dependency>
            <groupId>vw.interview</groupId>
            <artifactId>data</artifactId>
            <version>${project.version}</version>
        </dependency>

        <!-- Test modules -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-test</artifactId>
            <version>${spring.boot.starter.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-test</artifactId>
            <version>${spring.boot.starter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-test-autoconfigure</artifactId>
            <version>${spring.boot.starter.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.11</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework.batch</groupId>
            <artifactId>spring-batch-test</artifactId>
            <version>3.0.9.RELEASE</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.testcontainers</groupId>
            <artifactId>testcontainers</artifactId>
            <version>${org.testcontainers.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.testcontainers</groupId>
            <artifactId>postgresql</artifactId>
            <version>${org.testcontainers.version}</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.quartz-scheduler</groupId>
            <artifactId>quartz</artifactId>
            <version>2.3.0</version>
        </dependency>
    </dependencies>

    <build>
        <plugins>


            <plugin>
                <groupId>com.dkanejs.maven.plugins</groupId>
                <artifactId>docker-compose-maven-plugin</artifactId>
                <version>4.0.0</version>
                <executions>
                    <execution>
                        <id>pull</id>
                        <phase>test-compile</phase>
                        <goals>
                            <goal>pull</goal>
                        </goals>
                        <configuration>
                            <composeFile>${project.basedir}/../docker-compose.yml</composeFile>
                            <ignorePullFailures>true</ignorePullFailures>
                        </configuration>
                    </execution>
                    <execution>
                        <id>up</id>
                        <phase>test-compile</phase>
                        <goals>
                            <goal>up</goal>
                        </goals>
                        <configuration>
                            <composeFile>${project.basedir}/../docker-compose.yml</composeFile>
                            <detachedMode>true</detachedMode>
                        </configuration>
                    </execution>
                    <execution>
                        <id>down</id>
                        <phase>install</phase>
                        <goals>
                            <goal>down</goal>
                        </goals>
                        <configuration>
                            <composeFile>${project.basedir}/../docker-compose.yml</composeFile>
                            <removeVolumes>true</removeVolumes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <plugin>
                <groupId>com.github.slem1</groupId>
                <artifactId>await-maven-plugin</artifactId>
                <version>1.0</version>
                <executions>
                    <execution>
                        <phase>process-test-classes</phase>
                        <goals>
                            <goal>Await</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <poll>
                        <attempts>5</attempts>
                        <sleep>15000</sleep>
                    </poll>
                    <httpConnections>
                        <httpConnection>
                            <url>http://localhost:8081/v1/fota/</url>
                            <statusCode>200</statusCode>
                        </httpConnection>
                    </httpConnections>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0-M5</version>
                <dependencies>
                    <dependency>
                        <groupId>org.apache.maven.surefire</groupId>
                        <artifactId>surefire-junit47</artifactId>
                        <version>3.0.0-M5</version>
                    </dependency>
                </dependencies>
            </plugin>

            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>${spring.boot.starter.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>


        </plugins>
    </build>
</project>
