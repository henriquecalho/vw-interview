package com.vw.interview.admin;

import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

public abstract class AbstractJobLauncherDetails extends QuartzJobBean {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractJobLauncherDetails.class);

    @Override
    protected void executeInternal(final JobExecutionContext context) {

        LOGGER.info("[Job '{}' scheduled for current execution]", getJobName());
        BatchManager batchManager;

        try {
            batchManager = BatchManager.getInstance();
            batchManager.launchJob(getJobName(), context);
        } catch (Exception ex) {
            LOGGER.error("[Error occurred in batch manager]", ex);
        }

        LOGGER.info("[Job '{}' was successfully launched and executed!!!]", getJobName());
    }

    protected abstract String getJobName();
}
