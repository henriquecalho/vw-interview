package com.vw.interview.admin;

import org.quartz.JobExecutionContext;
import org.quartz.SchedulerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.configuration.JobLocator;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.context.ApplicationContext;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class BatchManager {

    private static volatile BatchManager instance = null;

    private static volatile Map<String,Long> runningJobs = new HashMap<>();

    public static final AtomicBoolean stopping = new AtomicBoolean( );

    private static final String APPLICATION_CONTEXT_FILE_KEY = "context";

    private static final Logger LOG = LoggerFactory.getLogger(BatchManager.class);

    public static BatchManager getInstance() {
        synchronized (BatchManager.class) {
            if (instance == null) {
                instance = new BatchManager();
            }
        }
        return instance;
    }

    private BatchManager() {
        // Singleton pattern is used
    }

    public void launchJob(final String jobName , final JobExecutionContext context) {
        ApplicationContext applicationContext;
        JobParameters jobParameters;
        SchedulerContext schedulerContext;
        JobLauncher jobLauncher;
        JobLocator jobLocator;
        Job job;
        JobExecution execution;

        try {
            LOG.info("[Job to be launched is '{}']", jobName);
            schedulerContext = context.getScheduler().getContext();
            applicationContext = (ApplicationContext) schedulerContext.get(APPLICATION_CONTEXT_FILE_KEY);
            jobLauncher = (JobLauncher) applicationContext.getBean("jobLauncher");
            jobLocator = (JobLocator) applicationContext.getBean("jobRegistry");
            jobParameters = new JobParametersBuilder().addLong("run.date", System.currentTimeMillis()).toJobParameters();
            job = jobLocator.getJob(jobName);

            LOG.info("[Job:'{}' will start running]", jobName);
            execution = jobLauncher.run(job, jobParameters);
            markJobAsCompleted(jobName, execution.getId());
            LOG.info("[Job:'{}';Execution status: {}]", jobName, execution.getStatus());
        } catch (Exception ex) {
            LOG.error(String.format("[Error executing job '%s'.]", jobName), ex);
        }
    }

    public void markJobAsCompleted(String jobName, Long jobExecutionId) {
        if(jobExecutionId.equals(runningJobs.get(jobName))) {
            runningJobs.remove(jobName);
        }
    }
}
