package com.vw.interview.exception;

public class BatchFailureException extends RuntimeException {

    public BatchFailureException() {
        super();
    }

    public BatchFailureException(String message) {
        super(message);
    }

    public BatchFailureException(Throwable ex) {
        super(ex);
    }

    public BatchFailureException(String message, Throwable ex) {
        super(message, ex);
    }

}
