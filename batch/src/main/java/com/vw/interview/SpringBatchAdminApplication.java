package com.vw.interview;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration;

@SpringBootApplication(exclude = FreeMarkerAutoConfiguration.class)
public class SpringBatchAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBatchAdminApplication.class, args);
    }
}