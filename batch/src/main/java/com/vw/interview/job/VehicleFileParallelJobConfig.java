package com.vw.interview.job;

import com.vw.interview.tasklet.parallel.HardwareFileParallelTasklet;
import com.vw.interview.tasklet.parallel.SoftwareFileParallelTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.support.SimpleFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@Configuration
public class VehicleFileParallelJobConfig {

    @Autowired
    private Environment environment;

    @Bean
    public Job vehicleFileParallelJob(JobBuilderFactory jobBuilders, StepBuilderFactory stepBuilders) {
        return jobBuilders
                .get("vehicleFileParallelJob")
                .start(splitFlow(stepBuilders))
                .build()        //builds FlowJobBuilder instance
                .build();       //builds Job instance
    }

    @Bean
    public Flow splitFlow(StepBuilderFactory stepBuilders) {
        return new FlowBuilder<SimpleFlow>("splitFlow")
                .split(taskExecutor())
                .add(flow1(stepBuilders), flow2(stepBuilders))
                .build();
    }

    @Bean
    public TaskExecutor taskExecutor(){
        return new SimpleAsyncTaskExecutor("spring_batch");
    }

    @Bean
    public Flow flow1(StepBuilderFactory stepBuilders) {
        return new FlowBuilder<SimpleFlow>("flow1")
                .start(softwareFileParallelStep(stepBuilders))
                .build();
    }

    @Bean
    public Flow flow2(StepBuilderFactory stepBuilders) {
        return new FlowBuilder<SimpleFlow>("flow2")
                .start(hardwareFileParallelStep(stepBuilders))
                .build();
    }

    @Bean
    public Step softwareFileParallelStep(StepBuilderFactory stepBuilders) {
        return stepBuilders
                .get("softwareFileParallelStep")
                .tasklet(softwareFileParallelTasklet())
                .build();
    }

    @Bean
    public Step hardwareFileParallelStep(StepBuilderFactory stepBuilders) {
        return stepBuilders
                .get("hardwareFileParallelStep")
                .tasklet(hardwareFileParallelTasklet())
                .build();
    }
    @Bean
    public SoftwareFileParallelTasklet softwareFileParallelTasklet() {
        return new SoftwareFileParallelTasklet(
                environment.getProperty("batch.files.path"),
                environment.getProperty("batch.files.archived.path"),
                environment.getProperty("batch.files.error.path"));
    }

    public HardwareFileParallelTasklet hardwareFileParallelTasklet() {
        return new HardwareFileParallelTasklet(
                environment.getProperty("batch.files.path"),
                environment.getProperty("batch.files.archived.path"),
                environment.getProperty("batch.files.error.path"));
    }
}