package com.vw.interview.job;

import com.vw.interview.tasklet.deep.HardwareFileDeepParallelTasklet;
import com.vw.interview.tasklet.deep.SoftwareFileDeepParallelTasklet;
import com.vw.interview.tasklet.deep.TempVehicleTableDeepParallelTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.job.flow.support.SimpleFlow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;

@Configuration
public class VehicleFileDeepParallelJobConfig {

    @Autowired
    private Environment environment;

    @Bean
    public Job vehicleFileDeepParallelJob(JobBuilderFactory jobBuilders, StepBuilderFactory stepBuilders) {
        return jobBuilders
                .get("vehicleFileDeepParallelJob")
                .start(splitDeepParallelFlow(stepBuilders))
                .next(tempVehicleTableDeepParallelStep(stepBuilders))
                .build()        //builds FlowJobBuilder instance
                .build();       //builds Job instance
    }

    @Bean
    public Flow splitDeepParallelFlow(StepBuilderFactory stepBuilders) {
        return new FlowBuilder<SimpleFlow>("splitDeepParallelFlow")
                .split(taskExecutor())
                .add(deepParallelSoftwareFlow(stepBuilders), deepParallelHardwareFlow(stepBuilders))
                .build();
    }

    @Bean
    public TaskExecutor taskExecutor(){
        return new SimpleAsyncTaskExecutor("spring_batch");
    }

    @Bean
    public Flow deepParallelSoftwareFlow(StepBuilderFactory stepBuilders) {
        return new FlowBuilder<SimpleFlow>("deepParallelSoftwareFlow")
                .start(softwareFileDeepParallelStep(stepBuilders))
                .build();
    }

    @Bean
    public Flow deepParallelHardwareFlow(StepBuilderFactory stepBuilders) {
        return new FlowBuilder<SimpleFlow>("deepParallelHardwareFlow")
                .start(hardwareFileDeepParallelStep(stepBuilders))
                .build();
    }

    @Bean
    public Step softwareFileDeepParallelStep(StepBuilderFactory stepBuilders) {
        return stepBuilders
                .get("softwareFileDeepParallelStep")
                .tasklet(softwareFileDeepParallelTasklet())
                .build();
    }

    @Bean
    public Step hardwareFileDeepParallelStep(StepBuilderFactory stepBuilders) {
        return stepBuilders
                .get("hardwareFileDeepParallelStep")
                .tasklet(hardwareFileDeepParallelTasklet())
                .build();
    }
    @Bean
    public SoftwareFileDeepParallelTasklet softwareFileDeepParallelTasklet() {
        return new SoftwareFileDeepParallelTasklet(
                environment.getProperty("batch.files.path"),
                environment.getProperty("batch.files.archived.path"),
                environment.getProperty("batch.files.error.path"));
    }

    public HardwareFileDeepParallelTasklet hardwareFileDeepParallelTasklet() {
        return new HardwareFileDeepParallelTasklet(
                environment.getProperty("batch.files.path"),
                environment.getProperty("batch.files.archived.path"),
                environment.getProperty("batch.files.error.path"));
    }

    @Bean
    public Step tempVehicleTableDeepParallelStep(StepBuilderFactory stepBuilders) {
        return stepBuilders
                .get("tempVehicleTableDeepParallelStep")
                .tasklet(tempVehicleTableTasklet())
                .build();
    }

    public TempVehicleTableDeepParallelTasklet tempVehicleTableTasklet() {
        return new TempVehicleTableDeepParallelTasklet(
                environment.getProperty("batch.files.path"),
                environment.getProperty("batch.files.archived.path"),
                environment.getProperty("batch.files.error.path"));
    }
}