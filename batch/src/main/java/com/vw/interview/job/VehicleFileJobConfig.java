package com.vw.interview.job;

import com.vw.interview.tasklet.simple.HardwareFileTasklet;
import com.vw.interview.tasklet.simple.SoftwareFileTasklet;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;

@Configuration
public class VehicleFileJobConfig {

    @Autowired
    private Environment environment;

    @Bean
    @Scheduled(cron = "* * * 3 *")
    public Job vehicleFileJob(JobBuilderFactory jobBuilders, StepBuilderFactory stepBuilders) {
        return jobBuilders
                .get("vehicleFileJob")
                .start(softwareFileStep(stepBuilders))
                .next(hardwareFileStep(stepBuilders))
                .build();
    }

    @Bean
    public Step softwareFileStep(StepBuilderFactory stepBuilders) {
        return stepBuilders
                .get("softwareFileStep")
                .tasklet(softwareFileTasklet())
                .build();
    }

    @Bean
    public Step hardwareFileStep(StepBuilderFactory stepBuilders) {
        return stepBuilders
                .get("hardwareFileStep")
                .tasklet(hardwareFileTasklet())
                .build();
    }

    @Bean
    public SoftwareFileTasklet softwareFileTasklet() {
        return new SoftwareFileTasklet(
                environment.getProperty("batch.files.path"),
                environment.getProperty("batch.files.archived.path"),
                environment.getProperty("batch.files.error.path"));
    }

    public HardwareFileTasklet hardwareFileTasklet() {
        return new HardwareFileTasklet(
                environment.getProperty("batch.files.path"),
                environment.getProperty("batch.files.archived.path"),
                environment.getProperty("batch.files.error.path"));
    }
}