package com.vw.interview.file;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.function.Predicate;

public class FileValidator implements Predicate<Path> {

    private final String prefix;
    private final String extension;

    public FileValidator(String prefix, String extension){
        this.prefix = prefix;
        this.extension = extension;
    }

    @Override
    public boolean test(Path path) {
        return Files.isRegularFile(path)
                && path.getFileName().toString().startsWith(prefix)
                && path.getFileName().toString().endsWith(extension);
    }
}
