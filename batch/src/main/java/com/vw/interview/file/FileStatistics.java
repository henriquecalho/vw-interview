package com.vw.interview.file;

import java.util.Date;

public class FileStatistics {

    private String name;
    private int vehiclesInFile;
    private int vehiclesInserted;
    private int codesInFile;
    private int codesInserted;
    private int vehicleCodesInFile;
    private int vehicleCodesInserted;
    private Date start;
    private Date end;

    public String getName() { return name; }
    public FileStatistics setName(String name) { this.name = name; return this; }

    public int getVehiclesInFile() { return vehiclesInFile; }

    public FileStatistics setVehiclesInFile(int vehiclesInFile) { this.vehiclesInFile = vehiclesInFile; return this; }

    public int getVehiclesInserted() { return vehiclesInserted; }

    public FileStatistics setVehiclesInserted(int vehiclesInserted) { this.vehiclesInserted = vehiclesInserted; return this; }

    public int getCodesInFile() { return codesInFile; }

    public FileStatistics setCodesInFile(int codesInFile) { this.codesInFile = codesInFile; return this; }

    public int getCodesInserted() { return codesInserted; }

    public FileStatistics setCodesInserted(int codesInserted) { this.codesInserted = codesInserted; return this; }

    public int getVehicleCodesInFile() { return vehicleCodesInFile; }

    public FileStatistics setVehicleCodesInFile(int vehicleCodesInFile) { this.vehicleCodesInFile = vehicleCodesInFile; return this; }

    public int getVehicleCodesInserted() { return vehicleCodesInserted; }

    public FileStatistics setVehicleCodesInserted(int vehicleCodesInserted) { this.vehicleCodesInserted = vehicleCodesInserted; return this; }

    public Date getStart() { return start; }

    public FileStatistics setStart(Date start) { this.start = start; return this; }

    public Date getEnd() { return end; }

    public FileStatistics setEnd(Date end) { this.end = end; return this; }

    public int incrementVehiclesInFile(int value) { vehiclesInFile+=value; return vehiclesInFile; }

    public int incrementVehiclesInserted(int value) { vehiclesInserted+=value; return vehiclesInserted; }

    public int incrementCodesInFile(int value) {codesInFile+=value;  return codesInFile; }

    public int incrementCodesInserted(int value) {codesInserted+=value;  return codesInserted; }

    public int incrementVehicleCodesInFile(int value) {vehicleCodesInFile+=value;  return vehicleCodesInFile; }

    public int incrementVehicleCodesInserted(int value) {vehicleCodesInserted+=value;  return vehicleCodesInserted; }

    @Override
    public String toString(){
        return System.lineSeparator() +
                "File statistics Description:" + System.lineSeparator() +
                "File: " + name + System.lineSeparator() +
                "Start: "  + start + System.lineSeparator() +
                "End: "  + end + System.lineSeparator() +
                vehiclesInFile + " Vehicles In File."  + System.lineSeparator() +
                vehiclesInserted + " Vehicles Inserted." + System.lineSeparator() +
                codesInFile + " Codes In File." + System.lineSeparator() +
                codesInserted + " Codes Inserted." + System.lineSeparator() +
                vehicleCodesInFile + " Vehicle Codes In File." + System.lineSeparator() +
                vehicleCodesInserted + " Vehicle Codes Inserted.";
    }
}
