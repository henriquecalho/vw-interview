package com.vw.interview.file;

public class FileEntry {
    private String vin;
    private String code;

    public FileEntry(String vin, String code){
        this.vin = vin;
        this.code = code;
    }

    public String getVin(){ return this.vin; }
    public FileEntry setVin(String vin)   { this.vin = vin;   return this; }
    public String getCode(){ return this.code; }
    public FileEntry setCode(String code) { this.code = code; return this; }
}
