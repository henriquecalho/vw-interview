package com.vw.interview.file.processor;

import com.vw.interview.file.FileEntry;
import com.vw.interview.file.FileStatistics;
import com.vw.interview.model.*;
import com.vw.interview.repository.CodeSubRepository;
import com.vw.interview.repository.Repository;
import com.vw.interview.repository.VehicleSubRepository;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class FileParallelProcessor extends FileProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(FileParallelProcessor.class);

    public FileParallelProcessor(String archivedDirectory, String errorDirectory, FileTypeEnum fileTypeEnum, CodeTypeEnum codeTypeEnum){
        super(archivedDirectory, errorDirectory, fileTypeEnum, codeTypeEnum);
    }

    @Override
    protected void processBlockOfLines(Repository repository, List<FileEntry> fileEntries, FileStatistics fileStatistics){
        Date now = new Date();
        repository.commitWork();
        // Insert with optimistic locking approach
        insertNewVehicles(repository, fileEntries, now, fileStatistics);
        insertNewCodes(repository, fileEntries, now, fileStatistics);
        insertNewVehicleCodes(repository, fileEntries, now, fileStatistics);
    }

    @Override
    protected void insertNewVehicles(Repository repository, List<FileEntry> fileEntries, Date now, FileStatistics fileStatistics){

        VehicleSubRepository vehicleRepository = (VehicleSubRepository) repository.vehicle();

        List<String> vins = fileEntries.stream().map(FileEntry::getVin).collect(Collectors.toList());
        // Remove duplicates
        vins = new ArrayList<>(new HashSet<>(vins));
        LOG.debug("{} +({}) Found unique vins in file.", fileStatistics.incrementVehiclesInFile(vins.size()), vins.size());

        // Remove vehicles already on the database
        vins.removeAll(vehicleRepository.get(vins));
        logNewEntities(new ArrayList<>(vins));

        // Insert in batches
        int i = 0;

        // Optimistic locking approach
        while (i < vins.size()) {
            try {
                repository.toSession(Session.class).beginTransaction();
                Vehicle vehicle = new Vehicle()
                        .setVin(vins.get(i))
                        .setCreationDate(now);
                repository.save(vehicle);

                // 50, same as the JDBC batch size
                // Flush a batch of inserts and release memory
                if (++i % FLUSH_THRESHOLD == 0)
                    repository.flushWork().clearWork();
            }
            catch (Exception e){
                handleOptimisticLockingConflict(repository, vins, i);
                i = 0;
            }
        }

        if(i > 0) repository.flushWork().clearWork();
        repository.commitWork();
        LOG.debug("{} +({}) Inserted vehicles.", fileStatistics.incrementVehiclesInserted(vins.size()), vins.size());
    }

    @Override
    protected void insertNewCodes(Repository repository, List<FileEntry> fileEntries, Date now, FileStatistics fileStatistics){
        CodeSubRepository codeRepository = (CodeSubRepository) repository.code();
        CodeType codeType = repository.get(CodeType.class, codeTypeEnum.id());
        String codeTypeId = codeTypeEnum.id();

        List<String> codes = fileEntries.stream().map(FileEntry::getCode).collect(Collectors.toList());
        // Remove duplicates
        codes = new ArrayList<>(new HashSet<>(codes));
        LOG.debug("{} +({}) {} Found codes in file.", fileStatistics.incrementCodesInFile(codes.size()), codes.size(), codeTypeId);

        // Remove codes already on the database
        codes.removeAll(codeRepository.get(codes));
        logNewEntities(new ArrayList<>(codes));

        // Insert in batches
        int i = 0;

        // Optimistic locking approach
        while (i < codes.size()) {
            try {
                Code code = new Code()
                        .setCodeType(codeType)
                        .setCodeId(codes.get(i))
                        .setCreationDate(now);
                repository.save(code);

                // 50, same as the jdbc batch size
                // Flush a batch of inserts and release memory
                if (++i % FLUSH_THRESHOLD == 0)
                    repository.flushWork().clearWork();
            }
            catch (Exception e){
                handleOptimisticLockingConflict(repository, codes, i);
                i = 0;
            }
        }

        if(i > 0) repository.flushWork().clearWork();
        repository.commitWork();
        LOG.debug("{} +({}) Inserted codes.", fileStatistics.incrementCodesInserted(codes.size()), codes.size());
    }

    @Override
    protected void insertNewVehicleCodes(Repository repository, List<FileEntry> fileEntries, Date now, FileStatistics fileStatistics){
        CodeSubRepository codeRepository = (CodeSubRepository) repository.code();
        VehicleSubRepository vehicleRepository = (VehicleSubRepository) repository.vehicle();

        List<VehicleCodes> vehicleConfigurationsList = fileEntries.stream()
                .map(entry -> new VehicleCodes()
                        .setVehicle(vehicleRepository.get(entry.getVin()))
                        .setCode(codeRepository.get(entry.getCode()))
                        .setCreationDate(now)
                )
                .collect(Collectors.toList());
        // Remove duplicates
        vehicleConfigurationsList = new ArrayList<>(new HashSet<>(vehicleConfigurationsList));
        int size = vehicleConfigurationsList.size();
        LOG.debug("{} +({}) Found vehicle codes in file.", fileStatistics.incrementVehicleCodesInFile(size), size);

        // Remove vehicleCodes already on the database
        List<VehicleCodes> alreadyPresent = codeRepository.getByVinAndCode(vehicleConfigurationsList);

        // Remove codes already on the database
        vehicleConfigurationsList.removeAll(alreadyPresent);
        logNewEntities(new ArrayList<>(vehicleConfigurationsList));

        // Insert in batches
        int i = 0;

        // Optimistic locking approach
        while (i < vehicleConfigurationsList.size()) {
            try {
                VehicleCodes vehicleCodes = vehicleConfigurationsList.get(i);
                repository.save(vehicleCodes);

                // 50, same as the jdbc batch size
                // Flush a batch of inserts and release memory
                if (++i % FLUSH_THRESHOLD == 0)
                    repository.flushWork().clearWork();
            }
            catch (Exception e){
                handleOptimisticLockingConflict(repository, vehicleConfigurationsList, i);
                i = 0;
            }
        }

        if(i > 0) repository.flushWork().clearWork();
        repository.commitWork();
        size = vehicleConfigurationsList.size();
        LOG.debug("{} +({}) Inserted vehicle codes.", fileStatistics.incrementVehicleCodesInserted(size), size);
    }

    private void handleOptimisticLockingConflict(Repository repository, List<?> entities, int i){
        LOG.debug("Optimistic locking approach. Removed: {}", entities.get(i));
        repository.toSession(Session.class).getTransaction().rollback();
        repository.toSession(Session.class).clear();
        repository.toSession(Session.class).close();
        entities.remove(i);
        optimisticLocks.incrementAndGet();
    }

    @Override
    protected FileModel createFile(Repository repository, Path path, FileStatistics fileStatistics){
        fileStatistics.setName(path.getFileName().toString());
        return repository.save(new FileModel()
                .setFileType(repository.get(FileType.class, fileTypeEnum.id()))
                .setFileStatus(repository.get(FileStatus.class, FileStatusEnum.CREATED.id()))
                .setName(path.getFileName().toString())
                .setPath(path.toString())
                .setCreationDate(new Date()));
    }
}
