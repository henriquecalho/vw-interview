package com.vw.interview.file.processor;

import com.vw.interview.file.FileEntry;
import com.vw.interview.file.FileStatistics;
import com.vw.interview.model.CodeTypeEnum;
import com.vw.interview.model.FileTypeEnum;
import com.vw.interview.model.FileVehicleTemp;
import com.vw.interview.repository.Repository;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class FileDeepParallelProcessor extends FileProcessor {

    public FileDeepParallelProcessor(String archivedDirectory, String errorDirectory, FileTypeEnum fileTypeEnum, CodeTypeEnum codeTypeEnum){
        super(archivedDirectory, errorDirectory, fileTypeEnum, codeTypeEnum);
    }

    @Override
    protected void processBlockOfLines(Repository repository, List<FileEntry> fileEntries, FileStatistics fileStatistics){
        int flushThreshold = 50;
        Session session = repository.toSession(Session.class);

        // Insert in batches
        int i;
        for (i=0; i < fileEntries.size(); ) {

            FileVehicleTemp fileVehicleTemp = new FileVehicleTemp()
                    .setFileTypeId(fileTypeEnum.id())
                    .setVin(fileEntries.get(i).getVin())
                    .setCodeId(fileEntries.get(i).getCode());

            session.save(fileVehicleTemp);

            // Flush a batch of inserts and release memory
            // 50, same as the JDBC batch size
            if (++i % flushThreshold == 0)
                repository.flushWork().clearWork();
        }
        if(i > 0) repository.flushWork().clearWork();
    }
}
