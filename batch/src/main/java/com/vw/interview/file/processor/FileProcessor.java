package com.vw.interview.file.processor;

import com.vw.interview.exception.DataException;
import com.vw.interview.file.FileEntry;
import com.vw.interview.file.FileStatistics;
import com.vw.interview.model.*;
import com.vw.interview.repository.CodeSubRepository;
import com.vw.interview.repository.Repository;
import com.vw.interview.repository.RepositoryFactory;
import com.vw.interview.repository.VehicleSubRepository;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

public class FileProcessor implements Consumer<Path> {

    private static final Logger LOG = LoggerFactory.getLogger(FileProcessor.class);
    protected static final int FLUSH_THRESHOLD = 50;
    protected final String archivedDirectory;
    protected final String errorDirectory;
    protected final FileTypeEnum fileTypeEnum;
    protected final CodeTypeEnum codeTypeEnum;
    /** Used for logging. */
    protected final Map<String, FileStatistics> fileStatisticsMap;
    protected final AtomicInteger optimisticLocks;

    public FileProcessor(String archivedDirectory, String errorDirectory, FileTypeEnum fileTypeEnum, CodeTypeEnum codeTypeEnum){
        this.archivedDirectory = archivedDirectory;
        this.errorDirectory = errorDirectory;
        this.fileTypeEnum = fileTypeEnum;
        this.codeTypeEnum = codeTypeEnum;
        this.fileStatisticsMap = new HashMap<>();
        this.optimisticLocks = new AtomicInteger();
    }

    @Override
    public void accept(Path path) {
        String key = path.toString();
        fileStatisticsMap.put(key, new FileStatistics());
        FileStatistics fileStatistics = fileStatisticsMap.get(key);

        LOG.info("Start processing file: {}", path);
        if(isDuplicatedFile(path)){
            LOG.info("File was already processed: {}", path);
            tryDeleteFile(path);
            return;
        }
        Repository repository = null;
        FileModel fileModel = null;
        try {
            repository = (Repository) RepositoryFactory.get().startNewWork();
            repository.toSession(Session.class).beginTransaction();
            fileStatistics.setStart(new Date());
            fileModel = createFile(repository, path, fileStatistics);
            tryProcessFile(repository, path);
            tryArchiveFile(path);
            updateFile(repository, fileModel, FileStatusEnum.ARCHIVED, archivedDirectory);
            repository.commitWork();
            fileStatistics.setEnd(new Date());
        }
        catch (Exception e) {
            LOG.error("Unexpected error when processing: {}", path, e);
            tryMoveToErrorDirectory(path);
            updateFile(repository, fileModel, FileStatusEnum.ERROR, errorDirectory);
            tryCommitWork(repository);
        }
        finally {
            tryFinishWork(repository);
        }
        LOG.info("Optimistic locks conflicts: {}", optimisticLocks.get());
        LOG.info("{}", fileStatistics);
    }

    protected void tryProcessFile(Repository repository, Path path){
        String key = path.toString();
        String csvFile = path.toString();
        List<FileEntry> fileEntries = new ArrayList<>();
        String cvsSplitBy = ",";
        String line;
        int blockSize = 1000;
        int lines = 0;

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while ((line = br.readLine()) != null) {

                fileEntries.add(tryParseLine(line, cvsSplitBy));

                // Read 1000 lines and process them to avoid memory overflow
                if(++lines % blockSize == 0){
                    processBlockOfLines(repository, fileEntries, fileStatisticsMap.get(key));
                    fileEntries = new ArrayList<>();
                }
            }
            processBlockOfLines(repository, fileEntries, fileStatisticsMap.get(key));
        }
        catch (IOException e) {
            LOG.error("Error reading file {}.", csvFile, e);
            tryMoveToErrorDirectory(path);
        }
        LOG.info("File processed {} lines: {}", lines, path);
    }

    /** Involve in a try catch so the process is max effort. */
    private FileEntry tryParseLine(String line, String cvsSplitBy){
        FileEntry fileEntry = null;
        try {
            String[] words = line.split(cvsSplitBy);
            fileEntry = new FileEntry(words[0], words[1]);
        }
        catch (Exception e){
            LOG.info("Unexpected error parsing line: {}", line);
        }
        return fileEntry;
    }

    protected void processBlockOfLines(Repository repository, List<FileEntry> fileEntries, FileStatistics fileStatistics){
        Date now = new Date();
        insertNewVehicles(repository, fileEntries, now, fileStatistics);
        insertNewCodes(repository, fileEntries, now, fileStatistics);
        insertNewVehicleCodes(repository, fileEntries, now, fileStatistics);
    }

    protected void insertNewVehicles(Repository repository, List<FileEntry> fileEntries, Date now, FileStatistics fileStatistics){

        VehicleSubRepository vehicleRepository = (VehicleSubRepository) repository.vehicle();
        Session session = repository.toSession(Session.class);

        List<String> vins = fileEntries.stream().map(FileEntry::getVin).collect(Collectors.toList());
        // Remove duplicates
        vins = new ArrayList<>(new HashSet<>(vins));
        LOG.debug("{} +({}) Found unique vins in file.", fileStatistics.incrementVehiclesInFile(vins.size()), vins.size());

        // Remove vehicles already on the database
        vins.removeAll(vehicleRepository.get(vins));
        logNewEntities(new ArrayList<>(vins));

        // Insert in batches
        int i;
        for (i=0; i < vins.size(); ) {
            Vehicle vehicle = new Vehicle()
                    .setVin(vins.get(i))
                    .setCreationDate(now);
            session.save(vehicle);

            // Flush a batch of inserts and release memory
            // 50, same as the JDBC batch size
            if (++i % FLUSH_THRESHOLD == 0)
                repository.flushWork().clearWork();
        }
        if(i > 0) repository.flushWork().clearWork();
        LOG.debug("{} +({}) Inserted vehicles.", fileStatistics.incrementVehiclesInserted(vins.size()), vins.size());
    }

    protected void insertNewCodes(Repository repository, List<FileEntry> fileEntries, Date now, FileStatistics fileStatistics){
        Session session = repository.toSession(Session.class);
        CodeSubRepository codeRepository = (CodeSubRepository) repository.code();
        CodeType codeType = repository.get(CodeType.class, codeTypeEnum.id());
        String codeTypeId = codeTypeEnum.id();

        List<String> codes = fileEntries.stream().map(FileEntry::getCode).collect(Collectors.toList());
        // Remove duplicates
        codes = new ArrayList<>(new HashSet<>(codes));
        LOG.debug("{} +({}) {} Found codes in file.", fileStatistics.incrementCodesInFile(codes.size()), codes.size(), codeTypeId);

        // Remove codes already on the database
        codes.removeAll(codeRepository.get(codes));
        logNewEntities(new ArrayList<>(codes));

        int i;
        // Insert in batches
        for (i=0; i < codes.size(); ) {
            Code code = new Code()
                    .setCodeType(codeType)
                    .setCodeId(codes.get(i))
                    .setCreationDate(now);
            session.save(code);

            // Flush a batch of inserts and release memory
            // 50, same as the jdbc batch size
            if (++i % FLUSH_THRESHOLD == 0)
                repository.flushWork().clearWork();
        }
        if(i > 0) repository.flushWork().clearWork();
        LOG.debug("{} +({}) Inserted codes.", fileStatistics.incrementCodesInserted(codes.size()), codes.size());
    }

    protected void insertNewVehicleCodes(Repository repository, List<FileEntry> fileEntries, Date now, FileStatistics fileStatistics){
        Session session = repository.toSession(Session.class);
        CodeSubRepository codeRepository = (CodeSubRepository) repository.code();
        VehicleSubRepository vehicleRepository = (VehicleSubRepository) repository.vehicle();

        List<VehicleCodes> vehicleConfigurationsList = fileEntries.stream()
                .map(entry -> new VehicleCodes()
                        .setVehicle(vehicleRepository.get(entry.getVin()))
                        .setCode(codeRepository.get(entry.getCode()))
                        .setCreationDate(now)
                )
                .collect(Collectors.toList());
        // Remove duplicates
        vehicleConfigurationsList = new ArrayList<>(new HashSet<>(vehicleConfigurationsList));
        int size = vehicleConfigurationsList.size();
        LOG.debug("{} +({}) Found vehicle codes in file.", fileStatistics.incrementVehicleCodesInFile(size), size);

        // Remove vehicleCodes already on the database
        List<VehicleCodes> alreadyPresent = codeRepository.getByVinAndCode(vehicleConfigurationsList);

        // Remove codes already on the database
        vehicleConfigurationsList.removeAll(alreadyPresent);
        logNewEntities(new ArrayList<>(vehicleConfigurationsList));

        int i;
        for (i=0; i < vehicleConfigurationsList.size(); ) {
            VehicleCodes vehicleCodes = vehicleConfigurationsList.get(i);
            session.saveOrUpdate(vehicleCodes);

            // Flush a batch of inserts and release memory
            // 50, same as the jdbc batch size
            if (++i % FLUSH_THRESHOLD == 0)
                repository.flushWork().clearWork();
        }
        if(i > 0) repository.flushWork().clearWork();
        size = vehicleConfigurationsList.size();
        LOG.debug("{} +({}) Inserted vehicle codes.", fileStatistics.incrementVehicleCodesInserted(size), size);
    }

    protected boolean isDuplicatedFile(Path path) {
        Repository repository = null;
        try {
            String fileName = path.getFileName().toString();
            repository = (Repository) RepositoryFactory.get().startNewWork();
            return repository.file().exists(fileName);
        }
        catch (DataException e){
            LOG.error("Error checking file.", e);
        }
        finally {
            tryFinishWork(repository);
        }
        return true;
    }

    protected FileModel createFile(Repository repository, Path path, FileStatistics fileStatistics){
        fileStatistics.setName(path.getFileName().toString());
        return repository.save(new FileModel()
                .setFileType(repository.get(FileType.class, fileTypeEnum.id()))
                .setFileStatus(repository.get(FileStatus.class, FileStatusEnum.CREATED.id()))
                .setName(path.getFileName().toString())
                .setPath(path.toString())
                .setCreationDate(new Date()));
    }

    protected void updateFile(Repository repository, FileModel fileModel, FileStatusEnum fileStatusEnum, String newDirectory){
        if(fileModel != null){
            String newPath = newDirectory + File.separator + fileModel.getName();
            repository.update(fileModel);
            fileModel.setFileStatus(repository.get(FileStatus.class, fileStatusEnum.id()))
                    .setPath(newPath);
        }
    }

    protected void tryArchiveFile(Path path){
        tryMoveFile(path, archivedDirectory);
    }

    protected void tryMoveToErrorDirectory(Path path){
        tryMoveFile(path, errorDirectory);
    }

    private void tryMoveFile(Path path, String toDirectory){
        try {
            Files.move(path, Paths.get(toDirectory + "/" + path.getFileName()));
            LOG.info("File moved to: {}", toDirectory);
        }
        catch (IOException e) {
            LOG.error("Error moving file {}.", path, e);
        }
    }

    protected void tryDeleteFile(Path path){
        try {
            Files.delete(path);
            LOG.info("File deleted: {}", path);
        }
        catch (IOException e) {
            LOG.error("Error deleting file {}.", path, e);
        }
    }

    protected void logNewEntities(List<Object> entities){
        if(LOG.isDebugEnabled())
            LOG.debug("{} Of which are not present on the database: {}", entities.size(), entities);
        else
            LOG.debug("{} Of which are not present on the database.", entities.size());
    }

    protected void tryFinishWork(Repository repository){ if(repository != null) repository.finishWork(); }

    protected void tryCommitWork(Repository repository){ if(repository != null) repository.commitWork(); }
}
