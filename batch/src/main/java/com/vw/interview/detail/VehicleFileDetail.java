package com.vw.interview.detail;

import com.vw.interview.admin.AbstractJobLauncherDetails;

public class VehicleFileDetail extends AbstractJobLauncherDetails {

    @Override
    protected String getJobName() { return "vehicleFileJob"; }
}
