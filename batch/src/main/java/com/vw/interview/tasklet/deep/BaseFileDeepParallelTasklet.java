package com.vw.interview.tasklet.deep;

import com.vw.interview.file.processor.FileDeepParallelProcessor;
import com.vw.interview.file.FileValidator;
import com.vw.interview.model.CodeTypeEnum;
import com.vw.interview.model.FileTypeEnum;
import com.vw.interview.tasklet.simple.BaseFileTasklet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BaseFileDeepParallelTasklet extends BaseFileTasklet {

    private static final Logger LOG = LoggerFactory.getLogger(BaseFileDeepParallelTasklet.class);

    public BaseFileDeepParallelTasklet(String filesDirectory, String archivedDirectory, String errorDirectory,
                                       FileTypeEnum fileTypeEnum, CodeTypeEnum codeTypeEnum, String prefix) {
        super(filesDirectory, archivedDirectory, errorDirectory, fileTypeEnum, codeTypeEnum, prefix);
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws IOException {
        String codeTypeId = codeTypeEnum.id();
        LOG.info("Start VehicleFileDeepParallelJob step: {}", codeTypeId);
        if(!inputDirectoryExits()){
            LOG.info("Directories not found.");
            createDirectories();
            return RepeatStatus.FINISHED;
        }

        List<Path> list;
        try (Stream<Path> paths = Files.list(Paths.get(filesDirectory))) {
            list = paths.collect(Collectors.toList());
        }

        list.parallelStream()
                .filter(new FileValidator(prefix, EXTENSION))
                .forEach(new FileDeepParallelProcessor(archivedDirectory, errorDirectory, fileTypeEnum, codeTypeEnum));

        LOG.info("Finish VehicleFileDeepParallelJob step: {}", codeTypeId);
        return RepeatStatus.FINISHED;
    }
}