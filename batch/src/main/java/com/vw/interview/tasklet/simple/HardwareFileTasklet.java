package com.vw.interview.tasklet.simple;

import com.vw.interview.model.CodeTypeEnum;
import com.vw.interview.model.FileTypeEnum;

public class HardwareFileTasklet extends BaseFileTasklet {

    public HardwareFileTasklet(String filesDirectory, String archivedDirectory, String errorDirectory) {
        super(filesDirectory, archivedDirectory, errorDirectory, FileTypeEnum.HARDWARE, CodeTypeEnum.HARDWARE, "hard_");
    }
}