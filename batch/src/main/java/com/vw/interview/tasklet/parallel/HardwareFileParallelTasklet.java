package com.vw.interview.tasklet.parallel;

import com.vw.interview.model.CodeTypeEnum;
import com.vw.interview.model.FileTypeEnum;

public class HardwareFileParallelTasklet extends BaseFileParallelTasklet {

    public HardwareFileParallelTasklet(String filesDirectory, String archivedDirectory, String errorDirectory) {
        super(filesDirectory, archivedDirectory, errorDirectory, FileTypeEnum.HARDWARE, CodeTypeEnum.HARDWARE, "hard_");
    }
}