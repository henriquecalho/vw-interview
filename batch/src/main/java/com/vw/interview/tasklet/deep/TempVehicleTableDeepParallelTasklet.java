package com.vw.interview.tasklet.deep;

import com.vw.interview.exception.DataException;
import com.vw.interview.model.*;
import com.vw.interview.repository.*;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.util.*;
import java.util.stream.Collectors;

public class TempVehicleTableDeepParallelTasklet implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger(TempVehicleTableDeepParallelTasklet.class);
    private static final int FLUSH_THRESHOLD = 50;
    private static final int PARTITION = 1000;
    protected final String filesDirectory;
    protected final String archivedDirectory;
    protected final String errorDirectory;

    public TempVehicleTableDeepParallelTasklet(String filesDirectory, String archivedDirectory, String errorDirectory) {
        this.filesDirectory = filesDirectory;
        this.archivedDirectory = archivedDirectory;
        this.errorDirectory = errorDirectory;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws DataException {
        LOG.info("Start tempVehicleTableDeepParallelJob step.");

        Date now = new Date();
        deleteFileVehicleTempDuplicates();
        insertVehicles(now);
        insertSoftwareCodes(now);
        insertHardwareCodes(now);
        insertVehicleCodesParallel(now);

        LOG.info("Finish tempVehicleTableDeepParallelJob.");
        return RepeatStatus.FINISHED;
    }

    private void deleteFileVehicleTempDuplicates() throws DataException {
        Repository repository = (Repository) RepositoryFactory.get();
        repository.startNewWork();
        IRepository.IFileSubRepository file = repository.file();
        repository.toSession(Session.class).beginTransaction();
        file.deleteFileVehicleTempDuplicates();
        repository.commitWork();
        repository.finishWork();
    }

    private void insertVehicles(Date now) {
        Repository repository = (Repository) RepositoryFactory.get();
        IRepository.IFileSubRepository file = repository.file();
        repository.toSession(Session.class).beginTransaction();
        file.insertDistinctVins();

        List<FileVehicleTempConflict> fileVehicleTempConflicts = file.getFileVehicleTempConflict();
        insertVehicles(repository, fileVehicleTempConflicts, now);

        file.deleteFileVehicleTempConflict();
        repository.commitWork();
    }

    private void insertVehicles(Repository repository, List<FileVehicleTempConflict> fileVehicleTempConflicts, Date now){

        VehicleSubRepository vehicleRepository = (VehicleSubRepository) repository.vehicle();

        List<String> vins = fileVehicleTempConflicts.stream().map(FileVehicleTempConflict::getTemp).collect(Collectors.toList());
        // Remove duplicates
        vins = new ArrayList<>(new HashSet<>(vins));

        // Remove vehicles already on the database
        vins.removeAll(vehicleRepository.get(vins));
        logNewEntities(new ArrayList<>(vins));

        // Insert in batches
        int i = 0;

        // Optimistic locking approach
        while (i < vins.size()) {
            try {
                repository.toSession(Session.class).beginTransaction();
                Vehicle vehicle = new Vehicle()
                        .setVin(vins.get(i))
                        .setCreationDate(now);
                repository.save(vehicle);

                // 50, same as the JDBC batch size
                // Flush a batch of inserts and release memory
                if (++i % FLUSH_THRESHOLD == 0)
                    repository.flushWork().clearWork();
            }
            catch (Exception e){
                handleOptimisticLockingConflict(repository, vins, i);
                i = 0;
            }
        }

        if(i > 0) repository.flushWork().clearWork();
        repository.commitWork();
    }

    private void insertSoftwareCodes(Date now) throws DataException {
        Repository repository = (Repository) RepositoryFactory.get();
        repository.startNewWork();
        IRepository.IFileSubRepository file = repository.file();
        repository.toSession(Session.class).beginTransaction();
        file.insertDistinctCodeIdsSoftware();

        List<FileVehicleTempConflict> fileVehicleTempConflicts = file.getFileVehicleTempConflict();
        insertNewCodes(repository, fileVehicleTempConflicts, now, CodeTypeEnum.SOFTWARE);

        file.deleteFileVehicleTempConflict();
        repository.commitWork();
        repository.finishWork();
    }

    private void insertHardwareCodes(Date now) throws DataException {
        Repository repository = (Repository) RepositoryFactory.get();
        repository.startNewWork();
        IRepository.IFileSubRepository file = repository.file();
        repository.toSession(Session.class).beginTransaction();
        file.insertDistinctCodeIdsHardware();

        List<FileVehicleTempConflict> fileVehicleTempConflicts = file.getFileVehicleTempConflict();
        insertNewCodes(repository, fileVehicleTempConflicts, now, CodeTypeEnum.HARDWARE);

        file.deleteFileVehicleTempConflict();
        repository.commitWork();
        repository.finishWork();
    }

    private void insertNewCodes(Repository repository, List<FileVehicleTempConflict> fileVehicleTempConflicts, Date now,
                                CodeTypeEnum codeTypeEnum){

        if(fileVehicleTempConflicts.isEmpty())
            return;
        CodeSubRepository codeRepository = (CodeSubRepository) repository.code();
        CodeType codeType = repository.get(CodeType.class, codeTypeEnum.id());

        List<String> codes = fileVehicleTempConflicts.stream().map(FileVehicleTempConflict::getTemp).collect(Collectors.toList());
        // Remove duplicates
        codes = new ArrayList<>(new HashSet<>(codes));

        // Remove codes already on the database
        codes.removeAll(codeRepository.get(codes));
        logNewEntities(new ArrayList<>(codes));

        // Insert in batches
        int i = 0;

        // Optimistic locking approach
        while (i < codes.size()) {

            try {
                Code code = new Code()
                        .setCodeType(codeType)
                        .setCodeId(codes.get(i))
                        .setCreationDate(now);
                repository.save(code);

                // 50, same as the jdbc batch size
                // Flush a batch of inserts and release memory
                if (++i % FLUSH_THRESHOLD == 0)
                    repository.flushWork().clearWork();
            }
            catch (Exception e){
                handleOptimisticLockingConflict(repository, codes, i);
                i = 0;
            }
        }

        if(i > 0) repository.flushWork().clearWork();
        repository.commitWork();
    }

    private void insertVehicleCodesParallel(Date now){
        Repository repository = (Repository) RepositoryFactory.get();
        IRepository.IFileSubRepository file = repository.file();
        int min = file.minFileVehicleTemp();
        int max = file.maxFileVehicleTemp();
        int size = max - min + 1;
        int fraction = size / 8;

        List<HashMap<String, Integer>> maps = new ArrayList<>();
        for (int i=min; i < max; i+=fraction){
            HashMap<String, Integer> map = new HashMap<>();
            map.put("min", i);
            map.put("max", i+fraction);
            maps.add(map);
        }

        maps.parallelStream()
                .forEach(map -> {
                    Repository repositoryIn = (Repository) RepositoryFactory.get();

                    int bottom = map.get("min");
                    int top = map.get("max") + PARTITION;
                    int windowMin = bottom;
                    int windowMax = windowMin + PARTITION;
                    if(windowMax > top) windowMax = map.get("max");

                    IRepository.IFileSubRepository fileIn = repositoryIn.file();
                    while (windowMin < top) {
                        List<FileVehicleTemp> fileVehicleTemps = fileIn.getFileVehicleTemp(windowMin, windowMax);
                        insertVehicleCodes(repositoryIn, fileVehicleTemps, now);
                        repositoryIn.toSession(Session.class).beginTransaction();
                        fileIn.deleteFileVehicleTemp(windowMin, windowMax);
                        repositoryIn.commitWork();
                        windowMin += PARTITION;
                        windowMax += PARTITION;
                        if(windowMax > top) windowMax = map.get("max");
                    }
                    repositoryIn.finishWork();
                });
        repository.finishWork();
    }

    private void insertVehicleCodes(Repository repository, List<FileVehicleTemp> fileVehicleTemps, Date now){

        CodeSubRepository codeRepository = (CodeSubRepository) repository.code();
        VehicleSubRepository vehicleRepository = (VehicleSubRepository) repository.vehicle();

        List<VehicleCodes> vehicleConfigurationsList = fileVehicleTemps.stream()
                .map(entry -> new VehicleCodes()
                        .setVehicle(vehicleRepository.get(entry.getVin()))
                        .setCode(codeRepository.get(entry.getCodeId()))
                        .setCreationDate(now)
                )
                .collect(Collectors.toList());

        // Remove duplicates
        vehicleConfigurationsList = new ArrayList<>(new HashSet<>(vehicleConfigurationsList));

        // Remove vehicleCodes already on the database
        List<VehicleCodes> alreadyPresent = codeRepository.getByVinAndCode(vehicleConfigurationsList);

        // Remove codes already on the database
        vehicleConfigurationsList.removeAll(alreadyPresent);
        logNewEntities(new ArrayList<>(vehicleConfigurationsList));

        // Insert in batches
        int i = 0;

        // Optimistic locking approach
        while (i < vehicleConfigurationsList.size()) {
            try {
                VehicleCodes vehicleCodes = vehicleConfigurationsList.get(i);
                repository.save(vehicleCodes);

                // 50, same as the jdbc batch size
                // Flush a batch of inserts and release memory
                if (++i % FLUSH_THRESHOLD == 0)
                    repository.flushWork().clearWork();
            }
            catch (Exception e){
                handleOptimisticLockingConflict(repository, vehicleConfigurationsList, i);
                i = 0;
            }
        }

        if(i > 0) repository.flushWork().clearWork();
        repository.commitWork();
    }

    private void handleOptimisticLockingConflict(Repository repository, List<?> entities, int i){
        LOG.debug("Optimistic locking approach. Removed: {}", entities.get(i));
        repository.toSession(Session.class).getTransaction().rollback();
        repository.toSession(Session.class).clear();
        repository.toSession(Session.class).close();
        entities.remove(i);
    }

    protected void logNewEntities(List<Object> entities){
        if(LOG.isDebugEnabled())
            LOG.debug("{} Of which are not present on the database: {}", entities.size(), entities);
        else
            LOG.debug("{} Of which are not present on the database.", entities.size());
    }
}