package com.vw.interview.tasklet.deep;

import com.vw.interview.model.CodeTypeEnum;
import com.vw.interview.model.FileTypeEnum;

public class SoftwareFileDeepParallelTasklet extends BaseFileDeepParallelTasklet {

    public SoftwareFileDeepParallelTasklet(String filesDirectory, String archivedDirectory, String errorDirectory) {
        super(filesDirectory, archivedDirectory, errorDirectory, FileTypeEnum.SOFTWARE, CodeTypeEnum.SOFTWARE, "soft_");
    }
}