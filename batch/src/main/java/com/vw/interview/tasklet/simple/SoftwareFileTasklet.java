package com.vw.interview.tasklet.simple;

import com.vw.interview.model.CodeTypeEnum;
import com.vw.interview.model.FileTypeEnum;

public class SoftwareFileTasklet extends BaseFileTasklet {

    public SoftwareFileTasklet(String filesDirectory, String archivedDirectory, String errorDirectory) {
        super(filesDirectory, archivedDirectory, errorDirectory, FileTypeEnum.SOFTWARE, CodeTypeEnum.SOFTWARE, "soft_");
    }
}