package com.vw.interview.tasklet.parallel;

import com.vw.interview.model.CodeTypeEnum;
import com.vw.interview.model.FileTypeEnum;

public class SoftwareFileParallelTasklet extends BaseFileParallelTasklet {

    public SoftwareFileParallelTasklet(String filesDirectory, String archivedDirectory, String errorDirectory) {
        super(filesDirectory, archivedDirectory, errorDirectory, FileTypeEnum.SOFTWARE, CodeTypeEnum.SOFTWARE, "soft_");
    }
}