package com.vw.interview.tasklet.deep;

import com.vw.interview.model.CodeTypeEnum;
import com.vw.interview.model.FileTypeEnum;

public class HardwareFileDeepParallelTasklet extends BaseFileDeepParallelTasklet {

    public HardwareFileDeepParallelTasklet(String filesDirectory, String archivedDirectory, String errorDirectory) {
        super(filesDirectory, archivedDirectory, errorDirectory, FileTypeEnum.HARDWARE, CodeTypeEnum.HARDWARE, "hard_");
    }
}