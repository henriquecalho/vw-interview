package com.vw.interview.tasklet.simple;

import com.vw.interview.file.processor.FileProcessor;
import com.vw.interview.file.FileValidator;
import com.vw.interview.model.CodeTypeEnum;
import com.vw.interview.model.FileTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BaseFileTasklet implements Tasklet {

    private static final Logger LOG = LoggerFactory.getLogger(BaseFileTasklet.class);
    protected final String filesDirectory;
    protected final String archivedDirectory;
    protected final String errorDirectory;
    protected static final String EXTENSION = "csv";
    protected final FileTypeEnum fileTypeEnum;
    protected final CodeTypeEnum codeTypeEnum;
    protected String prefix;

    public BaseFileTasklet(String filesDirectory, String archivedDirectory, String errorDirectory,
                           FileTypeEnum fileTypeEnum, CodeTypeEnum codeTypeEnum, String prefix) {
        this.filesDirectory = filesDirectory;
        this.archivedDirectory = archivedDirectory;
        this.errorDirectory = errorDirectory;
        this.fileTypeEnum = fileTypeEnum;
        this.codeTypeEnum = codeTypeEnum;
        this.prefix = prefix;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws IOException {
        String codeTypeId = codeTypeEnum.id();
        LOG.info("Start VehicleFileJob step: {}", codeTypeId);
        if(!inputDirectoryExits()){
            LOG.info("Directories not found.");
            createDirectories();
            return RepeatStatus.FINISHED;
        }

        List<Path> list;
        try (Stream<Path> paths = Files.list(Paths.get(filesDirectory))) {
            list = paths.collect(Collectors.toList());
        }

        list.stream()
                .filter(new FileValidator(prefix, EXTENSION))
                .forEach(new FileProcessor(archivedDirectory, errorDirectory, fileTypeEnum, codeTypeEnum));

        LOG.info("Finish VehicleFileJob step: {}", codeTypeId);
        return RepeatStatus.FINISHED;
    }

    protected boolean inputDirectoryExits(){
        return new File(filesDirectory).exists();
    }

    protected void createDirectories(){
        new File(filesDirectory).mkdirs();
        new File(filesDirectory + "/archived").mkdirs();
        new File(filesDirectory + "/error").mkdirs();
        LOG.info("File directories created.");
    }
}