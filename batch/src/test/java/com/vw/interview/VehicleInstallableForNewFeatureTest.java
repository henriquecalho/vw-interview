package com.vw.interview;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

import static org.assertj.core.api.Assertions.assertThat;

public class VehicleInstallableForNewFeatureTest {

    private static final String MAIN_ENDPOINT = "http://localhost:8081/v1/fota/";
    private static final String VIN = "VVV";
    private static final String FEATURE_ID = "FFF";
    private static final String CODE_ID = "CCCCCC";

    @Before
    public void before() throws IOException, SQLException {
        DatabaseUtil.createCode(CODE_ID);
        DatabaseUtil.createVehicle(VIN);
        DatabaseUtil.createFeature(FEATURE_ID, CODE_ID);
        DatabaseUtil.createVehicleCode(VIN, CODE_ID);
    }

    @After
    public void after() throws IOException, SQLException {
        DatabaseUtil.deleteVehicleCodes(VIN, CODE_ID);
        DatabaseUtil.deleteFeature(FEATURE_ID);
        DatabaseUtil.deleteCode(CODE_ID);
        DatabaseUtil.deleteVehicle(VIN);
    }

    @Test
    public void testVehicleInstallableForNewFeature() throws IOException, SQLException {
        assertThat(requestInstallableFeatures(VIN)).contains(FEATURE_ID);
    }

    private String requestInstallableFeatures(String vin) throws IOException {
        String endpoint = MAIN_ENDPOINT + String.format("vehicles/%s/installable/", vin);
        return HttpUtil.sendGet(endpoint);
    }
}
