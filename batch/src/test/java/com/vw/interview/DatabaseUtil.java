package com.vw.interview;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseUtil {

    public static final String POSTGRES_CONFIG_PROPERTIES = "postgres-config.properties";

    public static void createCode(String codeId) throws IOException, SQLException {
        String insert = "insert into code (code_id, code_type_id) values (?, 'HARDWARE')";
        executePreparedStatement(insert, codeId);
    }

    public static void createVehicle(String vin) throws IOException, SQLException {
        String insert = "insert into vehicle (vin) values (?)";
        executePreparedStatement(insert, vin);
    }

    public static void createFeature(String featureId, String codeId) throws IOException, SQLException {
        String insert = "insert into feature (feature_id) values (?)";
        executePreparedStatement(insert, featureId);

        String insertFeatureDetail = "insert into feature_detail " +
                "(feature_id, code_id, feature_detail_existence_id) " +
                "values (?, ?, 'REQUIRED')";
        executePreparedStatement(insertFeatureDetail, featureId, codeId);
    }

    public static void createVehicleCode(String vin, String codeId) throws IOException, SQLException {
        String insert = "insert into vehicle_codes (vehicle_id, code_id) " +
                "select vehicle_id, ? " +
                "from vehicle " +
                "where vin = ?";
        executePreparedStatement(insert, codeId, vin);
    }

    public static void deleteVehicleCodes(String vin, String codeId) throws SQLException, IOException {
        String delete = "delete from vehicle_codes where code_id = ? and vehicle_id = (select vehicle_id from vehicle where vin = ?)";
        executePreparedStatement(delete, codeId, vin);
    }

    public static void deleteCode(String codeId) throws SQLException, IOException {
        String delete = "delete from code where code_id = ?";
        executePreparedStatement(delete, codeId);
    }

    public static void deleteVehicle(String vin) throws SQLException, IOException {
        String delete = "delete from vehicle where vin = ?";
        executePreparedStatement(delete, vin);
    }

    public static void deleteFeature(String featureId) throws SQLException, IOException {
        String deleteFeatureDetail = "delete from feature_detail where feature_id = ?";
        String deleteFeature = "delete from feature where feature_id = ?";
        executePreparedStatement(deleteFeatureDetail, featureId);
        executePreparedStatement(deleteFeature, featureId);
    }

    public static void executePreparedStatement(String sql, String... params) throws SQLException, IOException {
        Properties config = getProperties();
        try (Connection connection = DriverManager.getConnection(
                config.getProperty("url"),
                config.getProperty("user"),
                config.getProperty("password"));
             PreparedStatement statement = connection.prepareStatement(sql)
        ){
            int i = 1;
            for(String param : params){
                statement.setString(i++, param);
            }
            statement.execute();
        }
    }

    public static Properties getProperties() throws IOException {
        Properties properties = new Properties();
        try (InputStream in = VehicleIncompatibleForNewFeatureTest
                .class
                .getClassLoader()
                .getResourceAsStream(POSTGRES_CONFIG_PROPERTIES)) {
            properties.load(in);
        }
        return properties;
    }
}
