
-- DROPS

drop table feature_detail;
drop table feature;
drop table file;
drop table feature_detail_existence;
drop table file_status;
drop table file_type;
drop table vehicle_codes;
drop table code;
drop table code_type;
drop table vehicle;
drop table file_vehicle_temp;
drop table file_vehicle_temp_conflict;

drop table qrtz_blob_triggers;
drop table qrtz_calendars;
drop table qrtz_cron_triggers;
drop table qrtz_fired_triggers;
drop table qrtz_locks;
drop table qrtz_paused_trigger_grps;
drop table qrtz_scheduler_state;
drop table qrtz_simple_triggers;
drop table qrtz_simprop_triggers;
drop table qrtz_triggers;
drop table qrtz_job_details;

delete from databasechangelog;
