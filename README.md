
**Hi. Welcome to the readme of the interview assignment.**  
**In here you'll find:**

1. Database model description
2. Project description
3. External libraries used
4. How to build the project
5. How to build the containers and deploy
6. Connect to the database
7. How to execute
8. How to execute the tests

---

## 1. Database model description

UML available at /resource/uml/uml.png

- **vehicle** Vehicle information
- **code_type** Available code types: SOFTWARE, HARDWARE (REFDATA)
- **code** Code information
- **vehicle_codes** Vehicle and Code join tables
- **feature** Feature information
- **feature_detail** Codes present in a feature
- **feature_detail_existence** Available types of restriction for a code: REQUIRED, ABSENT (REFDATA)
- **file_type** Available file types: SOFTWARE, HARDWARE (REFDATA)
- **file_status** Available file status: CREATE, ARCHIVED, ERROR (REFDATA)
- **file** File information
- **file_vehicle_temp** Temporary table. Contains the content of the file.
- **file_vehicle_temp_conflict** Temporary table. Contains information to be added in batch. Used to avoid concurrency conflicts.
- **batch_job_execution** Spring Data auto generated tables
- **batch_job_execution_context**
- **batch_job_execution_params**
- **batch_job_instance**
- **batch_step_execution**
- **batch_step_execution_context**
- **qrtz_blob_triggers** Quartz tables
- **qrtz_calendars**
- **qrtz_cron_triggers**
- **qrtz_fired_triggers**
- **qrtz_locks**
- **qrtz_paused_trigger_grps**
- **qrtz_scheduler_state**
- **qrtz_simple_triggers**
- **qrtz_simprop_triggers**
- **qrtz_triggers**
- **qrtz_job_details**

---

## 2. Project description

It consists of 3 modules:

- **data**

Responsibility of interacting with the data model.

- **api**

API provided with the following endpoints:

/v1/fota/ - gives all the endpoints  
/v1/fota/vehicles/{vin}/installable - gives all the features that can be installed for the corresponding vin  
/v1/fota/vehicles/{vin}/incompatible - gives all the features that cannot be installed for the corresponding vin

- **batch**

Spring Batch module for execution batch processes.  
It contains 3 jobs. They're all different implementations of the requested process.  
I took the liberty of providing different implementations so that we can have more to discuss if you so desire.

1. **vehicleFileJob** - Process each file sequentially. On my container it took ~57 minutes to process all the given files from an empty database. If vehicles and codes were already on the database makes no difference.
2. **vehicleFileParallelJob** - Process all files in parallel. On my container it took ~20 minutes to process all the given files from an empty database. If vehicles and codes were already on the database it took 2 minutes.
3. **vehicleFileDeepParallelJob** - Copy  in parallel all files data to a temporary table. Process the temporary table data in parallel. On my container it took ~50 seconds to process all the given files from an empty database. If vehicles and codes were already on the database it took 40 seconds.

---

## 3. External libraries used

- **hibernate** Accessing database and mapping data
- **querydsl** Creating type-safe queries in a domain specific language that is similar to SQL
- **jackson** Json parser
- **antrun** Generate data objects from database
- **liquibase** Execute sql scripts automatically

---

## 4. How to build the project

-- with tests
mvn clean install

-- without tests
mvn clean install -DskipTests

---

## 5. How to build the containers and deploy

docker-compose build && docker-compose up  
(database schema will be created automatically through liquibase) 

Three containers are present:
- postgres database
- api web application
- batch web application

---

## 6. Connect to the database

The database is deployed in a container.

jdbc:postgresql://man_db_1:5432/tc  
username: tc  
password: tc  

The database schema will be created automatically, and the data inserted by liquibase.  
The scripts can be found at:
\data\src\main\resources\db\changelog\migrations

---

## 7. How to execute

Two web applications are available: 

 - **api** http://localhost:8081/v1/fota/vehicles/
 - **batch** http://localhost:8082/jobs

To execute the batch process:

1. Select the job
2. Enter the parameters: run.id=1 (must be unique, so if you're executing manually, you need to increment it)
3. Launch (the first execution will create the missing directories)
4. Copy a file to your container: docker cp .\resources\files\hard_001.csv man_batch_1:\batch\files\hard_001.csv
5. Launch

**Additional info:**  
If you need to access the container:   
docker exec -it man_batch_1 /bin/ash  

Beware that the same file can only be processed once.  
If you want to do it you must delete it from the file system and delete its entry in the database:  
delete from file where file_name = ?;  

Files can be in following directories:

 - **\bach\files** Files to be processed
 - **\bach\files\archived** Processed files
 - **\bach\files\error** Error files

---

## 8. How to execute the tests

mvn test

**Note:**  
Unfortunately I couldn't do all the tests I wanted.  
It would require me some time to configure the containers for the tests.  
I left this to the end and time ran out. I'm sorry.  
I ended up just doing some simple ones against the api and for that you're required to have the container running.
