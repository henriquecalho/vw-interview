package com.vw.interview.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.vw.interview.exception.DataException;
import com.vw.interview.model.Feature;
import com.vw.interview.repository.IRepository;
import com.vw.interview.repository.RepositoryFactory;
import com.vw.interview.util.JsonConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(value = "/vehicles")
public class VehicleController {

    private static final Logger LOG = LoggerFactory.getLogger(VehicleController.class);

    @GetMapping(value = "/{vin}/installable/", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Feature>> getVehicleInstallableFeatures(@PathVariable("vin") String vin) {
        IRepository repository;
        List<Feature> features;
        ResponseEntity<List<Feature>> response = null;
        try {
            repository = RepositoryFactory.get().startNewWork();
            features = repository.feature().getVehicleInstallableFeatures(vin);
            if(features.isEmpty())
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not Found");
            response = new ResponseEntity<>(features, HttpStatus.OK);
        }
        catch (DataException e) {
            LOG.error("Unexpected databases error for vin {}.", vin, e);
        }
        return response;
    }

    @GetMapping(value = "/{vin}/incompatible/", produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Feature>> getVehicleIncompatibleFeatures(@PathVariable("vin") String vin) {
        IRepository repository;
        List<Feature> features;
        ResponseEntity<List<Feature>> response = null;
        try {
            repository = RepositoryFactory.get().startNewWork();
            features = repository.feature().getVehicleIncompatibleFeatures(vin);
            if(features.isEmpty())
                throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Not Found");
            response = new ResponseEntity<>(features, HttpStatus.OK);
        }
        catch (DataException e) {
            LOG.error("Unexpected databases error for vin {}.", vin, e);
        }
        return response;
    }
}