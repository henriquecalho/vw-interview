package com.vw.interview.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class HomeController {

    @Autowired
    private final RequestMappingHandlerMapping handlerMapping;

    @Autowired
    public HomeController(RequestMappingHandlerMapping handlerMapping) {
        this.handlerMapping = handlerMapping;
    }

    @GetMapping(value="/", produces= MediaType.APPLICATION_JSON_VALUE)
    public List<String> getAllEndpoints() {
        return handlerMapping
                .getHandlerMethods()
                .keySet()
                .stream()
                .map(Object::toString)
                .collect(Collectors.toList());
    }
}